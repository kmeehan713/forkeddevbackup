//Combine the Spectra From multiple event configurations
//by taking a weighted average for each point in the spectra.

#include <iostream>
#include <vector>
#include <utility>

#include <TStyle.h>
#include <TMath.h>
#include <TFile.h>
#include <TF1.h>
#include <TCanvas.h>
#include <TSystem.h>

#include "globalDefinitions.h"
#include "utilityFunctions.h"
#include "StRefMultExtendedCorr.h"
#include "UserCuts.h"
#include "ParticleInfo.h"
#include "SpectraClass.h"
#include "SpectraFitUtilities.h"
#include "SpectraFitFunctions.h"
#include "StyleSettings.h"

bool draw = false;

void HighEnergyMidRapidityException(const unsigned int centBin, const unsigned int midYIndex,
				    double mTm0, TGraphErrors *spectrum,
				    std::vector< std::vector < std::vector < SpectraClass *> > > *spectraClass);

void combineSpectra(const int pid, const int charge, const unsigned int midYIndex, TString resultsFile,
		    TString fileCenter, TString filePosY="", TString fileNegY=""){

  
  ParticleInfo *particleInfo = new ParticleInfo();
  const unsigned int nCentBins = GetNCentralityBins();
  const double energy = GetCollisionEnergy();

  //Create a Vector of the Files
  std::vector<TFile *> files;
  if (!fileCenter.CompareTo("")){
    cout <<"ERROR - CombineSpectra() - Inputfile has not been specified. EXITING!" <<endl;
    exit (EXIT_FAILURE);
  }
  else
    files.push_back(new TFile(fileCenter,"READ"));
  if (filePosY.CompareTo(""))
    files.push_back(new TFile(filePosY,"READ"));
  if (fileNegY.CompareTo(""))
    files.push_back(new TFile(fileNegY,"READ"));
  const unsigned int nConfigs = files.size(); 
  
  //Directory Names in Each of the Files
  TString spectraClassDir = TString::Format("SpectraClass_%s",
					    particleInfo->GetParticleName(pid,charge).Data());
  TString spectrumDir = TString::Format("CorrectedSpectra_%s",
					particleInfo->GetParticleName(pid,charge).Data());

  TCanvas *canvas = NULL;
  if (draw)
    canvas = new TCanvas("canvas","canvas",20,20,800,600);

  //--------------------------------------------------------
  //Get all of the Spectra Class Objects
  cout <<"Getting the SpectraClass Objects" <<endl;
  std::vector< std::vector< std::vector< SpectraClass *> > > spectraClass
    (nConfigs, std::vector< std::vector <SpectraClass *> >
     (nCentBins, std::vector< SpectraClass *>
      (nRapidityBins, (SpectraClass *) NULL)));
  for (unsigned int iConfig=0; iConfig<nConfigs; iConfig++){
    for (unsigned int iCentBin=0; iCentBin<nCentBins; iCentBin++){
      for (unsigned int yIndex=0; yIndex<nRapidityBins; yIndex++){

	spectraClass.at(iConfig).at(iCentBin).at(yIndex) = (SpectraClass *)
	  files.at(iConfig)->Get(Form("%s/CorrectedSpectra_%s_Cent%02d_yIndex%02d_Total",
				     spectraClassDir.Data(),
				     particleInfo->GetParticleName(pid,charge).Data(),
				     iCentBin,yIndex));
	
	if (!spectraClass.at(iConfig).at(iCentBin).at(yIndex))
	  continue;

	spectraClass.at(iConfig).at(iCentBin).at(yIndex)->SetSpectraFile(files.at(iConfig));
	spectraClass.at(iConfig).at(iCentBin).at(yIndex)->SetSpectraClassDir(spectraClassDir);
	spectraClass.at(iConfig).at(iCentBin).at(yIndex)->SetSpectraDir(spectrumDir);
	spectraClass.at(iConfig).at(iCentBin).at(yIndex)->LoadSpectra();
	
	//Set Marker Style in Case of Drawing
	if (spectraClass.at(iConfig).at(iCentBin).at(yIndex)->GetTPCSpectrum()){
	  spectraClass.at(iConfig).at(iCentBin).at(yIndex)->GetTPCSpectrum()->SetMarkerColor(kGray);
	}
	if (spectraClass.at(iConfig).at(iCentBin).at(yIndex)->GetTOFSpectrum()){
	  spectraClass.at(iConfig).at(iCentBin).at(yIndex)->GetTOFSpectrum()->SetMarkerColor(kGray);
	}
	
      }//End Loop Over Rapidity Bins
      
    }//End Loop Over Cent Bins
    
  }//End Loop Over Configs
  cout <<"SpectraClass Objects have been loaded" <<endl;
  
  //Create the Results File that will hold the output
  TFile *outFile = new TFile(resultsFile,"UPDATE");
  
  //----------------------------------------------------------
  //Combine All of the Spectra from the different event configs  
  for (unsigned int iCentBin=0; iCentBin<nCentBins; iCentBin++){
    for (unsigned int yIndex=0; yIndex<=midYIndex; yIndex++){
 
      const unsigned int yIndexHigh = midYIndex + yIndex;
      const unsigned int yIndexLow = midYIndex - yIndex;
      const double rapidity = fabs(GetRapidityRangeCenter(yIndexHigh));

      //Organize the Spectra to combine
      std::vector<SpectraClass *> spectraToCombine;

      //The Center Config has two spectra except at mid rapidity
      if (spectraClass.at(0).at(iCentBin).at(yIndexHigh))
	spectraToCombine.push_back(spectraClass.at(0).at(iCentBin).at(yIndexHigh));
      if (yIndexHigh != yIndexLow && spectraClass.at(0).at(iCentBin).at(yIndexLow))
	spectraToCombine.push_back(spectraClass.at(0).at(iCentBin).at(yIndexLow));
      
      //The PosY and NegY Files each have one spectrum
      if (nConfigs > 1)
	if (spectraClass.at(1).at(iCentBin).at(yIndexHigh))
	  spectraToCombine.push_back(spectraClass.at(1).at(iCentBin).at(yIndexHigh));
      if (nConfigs > 2)
	if (spectraClass.at(2).at(iCentBin).at(yIndexLow))
	  spectraToCombine.push_back(spectraClass.at(2).at(iCentBin).at(yIndexLow));
 
      //There must be at least one spectrum
      if (spectraToCombine.size() == 0)
	continue;
 
      //The only way there should be only one spectrum is if there is only one
      //configuration and the current rapidity bin is mid rapidity
      if (spectraToCombine.size() == 1){
	if (yIndexHigh != yIndexLow && nConfigs == 1)
	  continue;
      }
      
      //Create a temp spectrum to hold the combined spectrum
      TGraphErrors *tempSpectrum = new TGraphErrors();
      tempSpectrum->SetName(Form("CombinedSpectrum_%s_Cent%02d_yIndex%02d",
				 particleInfo->GetParticleName(pid,charge).Data(),
				 iCentBin,yIndexHigh));
      tempSpectrum->SetMarkerColor(particleInfo->GetParticleColor(pid));
      tempSpectrum->SetMarkerStyle(particleInfo->GetParticleMarker(pid,charge));

      //Loop Over the Points of the spectra, for each mTm0 bin compute the weighted
      //average of the bin and compute the error.
      for (unsigned int mTm0Index=0; mTm0Index<nmTm0Bins; mTm0Index++){

	double mTm0 = GetmTm0RangeCenter(mTm0Index);

	std::vector<double> xVal, yVal, xErr, yErr;
	for (unsigned int iSpectrum=0; iSpectrum<spectraToCombine.size(); iSpectrum++){

	  //Get the Nearest Point
	  int nP = FindElementWithMinDistance(spectraToCombine.at(iSpectrum)->GetN(),
					      spectraToCombine.at(iSpectrum)->GetX(),
					      mTm0);

	  //Make sure the point is within this mTm0 bin
	  if (fabs(spectraToCombine.at(iSpectrum)->GetX()[nP] - mTm0) > mTm0BinWidth/2.0)
	    continue;

	  xVal.push_back(spectraToCombine.at(iSpectrum)->GetX()[nP]);
	  yVal.push_back(spectraToCombine.at(iSpectrum)->GetY()[nP]);

	  //The Weights are 1/err^2
	  xErr.push_back(1.0 / pow(spectraToCombine.at(iSpectrum)->GetEX()[nP],2));
	  yErr.push_back(1.0 / pow(spectraToCombine.at(iSpectrum)->GetEY()[nP],2));
	  
	}//End Loop Over Spectra

	if (xVal.size() == 0)
	  continue;
		
	//In general, we want to require that there is at least two measurements
	//for each transverse mass bin. There is an important exception though.
	//For collision energies above 19.6, there is only one example of the mid
	//rapidity spectrum and thus only one point is measured for a given transverse
	//mass bin. The logic below accounts for this exception.

	//If there is at least two values to combine for this transverse mass bin
	//then do the default weighted average.
	if (xVal.size() >= 2){	
	  
	  //Compute the Weighted Averages
	  double avgX = TMath::Mean(xVal.size(),&xVal.at(0),&xErr.at(0));
	  double avgY = TMath::Mean(yVal.size(),&yVal.at(0),&yErr.at(0));
	  double rmsX = TMath::RMS(xVal.size(),&xVal.at(0),&xErr.at(0));
	  double rmsY = TMath::RMS(yVal.size(),&yVal.at(0),&yErr.at(0));
	  rmsX = rmsX/sqrt((double)xVal.size());
	  rmsY = rmsY/sqrt((double)yVal.size());

	  //Compute the Error on the Weighted Averages
	  double errX(0), errY(0);
	  for (unsigned int i=0; i<xVal.size(); i++){
	    errX += xErr.at(i);
	    errY += yErr.at(i);
	  }
	  errX = sqrt(1.0 / errX);
	  errY = sqrt(1.0 / errY);
	  
	  //Compute the Total Error
	  double totalErrX = sqrt(pow(rmsX,2) + pow(errX,2));
	  double totalErrY = sqrt(pow(rmsY,2) + pow(errY,2));
	  
	  tempSpectrum->SetPoint(tempSpectrum->GetN(),avgX,avgY);
	  tempSpectrum->SetPointError(tempSpectrum->GetN()-1,errX,errY);
	}//End If there are at least two values to combine

	//If there is only one value, the collision energy is higher than 19.6,
	//and this is the mid rapidity index, then do this
	else if (xVal.size() == 1 && yIndexLow == yIndexHigh){
	  HighEnergyMidRapidityException(iCentBin,yIndexHigh,mTm0,tempSpectrum,
					 &spectraClass);
	}

	
      }//End Loop Over mTm0Bins      

      //Make Sure the Combined Spectrum has Points
      if (tempSpectrum->GetN() == 0){
	delete tempSpectrum;
	for (unsigned int iSpectrum=0; iSpectrum<spectraToCombine.size(); iSpectrum++){
	  if (spectraToCombine.at(iSpectrum))
	    delete spectraToCombine.at(iSpectrum);
	}
	continue;
      }
      
      //Create a new SpectraClass object to store the combined spectrum in
      SpectraClass *combinedSpectrum = new SpectraClass(pid,charge,iCentBin,yIndexHigh,
							tempSpectrum);
      combinedSpectrum->SetSpectraFile(outFile);
      combinedSpectrum->SetSpectraDir(Form("CombinedCorrectedSpectra_%s",
					   particleInfo->GetParticleName(pid,charge).Data()));
      combinedSpectrum->SetSpectraClassDir(Form("CombinedCorrectedSpectraClass_%s",
						particleInfo->GetParticleName(pid,charge).Data()));

      //Set the Bin Width Vector
      std::vector<double> binWidthVec;
      for (int iPoint=0; iPoint<combinedSpectrum->GetN(); iPoint++){

	double mTm0 = combinedSpectrum->GetX()[iPoint];

	int nP = FindElementWithMinDistance(spectraToCombine.at(0)->GetN(),
					    spectraToCombine.at(0)->GetX(),
					    mTm0);

	binWidthVec.push_back(spectraToCombine.at(0)->GetBinWidthVector()->at(nP));
	
      }
      combinedSpectrum->SetBinWidthVector(binWidthVec);

      //Write the Combined Spectrum
      combinedSpectrum->WriteSpectrum(true,true);

      
      //Optional Draw
      if (draw){
	canvas->cd();
	combinedSpectrum->DrawSpectrum("APZ");
	for (unsigned int i=0; i<spectraToCombine.size(); i++){
	  spectraToCombine.at(i)->Draw("PZ");
	}
	combinedSpectrum->DrawSpectrum("PZ");

	canvas->Update();
	gSystem->Sleep(500);
	
      }//End Optional Draw

      
      //Clean Up
      delete tempSpectrum;
      delete combinedSpectrum;
      for (unsigned int iSpectrum=0; iSpectrum<spectraToCombine.size(); iSpectrum++){
	if (spectraToCombine.at(iSpectrum))
	  delete spectraToCombine.at(iSpectrum);
      }
      spectraToCombine.clear();
 
    }//End Loop Over Rapidity Bins

    
  }//End Loop Over Centrality Bins

}

//__________________________________________________________________________________________
void HighEnergyMidRapidityException(const unsigned int centBin, const unsigned int midYIndex,
				    double mTm0, TGraphErrors *spectrum,
				    std::vector< std::vector < std::vector < SpectraClass *> > > *spectraClass){


  //This computes the error on a point in a spectrum for which there is only one instance of
  //a measurement available. It does this by looking at the same mTm0 point in adjacent spectra,
  //computing the percent error, and then combining that percent error in quadrature with
  //the error currently on the mid-rapidity point.

  //WARNING!!! - THIS FUNCTION ASSUMES THAT THE SPECTRUM OF INTEREST IS FROM THE COLLIDERCENTER CONFIGURATION.

  //Get the spectrum of interest, which is assumed to be at config=0 
  SpectraClass *currentSpectrum = spectraClass->at(0).at(centBin).at(midYIndex);

  //Make Sure the current spectrum has a point for this mTm0 bin
  int cP =  FindElementWithMinDistance(currentSpectrum->GetN(),
				       currentSpectrum->GetX(),
				       mTm0);
  if (fabs(currentSpectrum->GetX()[cP] - mTm0) > mTm0BinWidth/2.0)
    return;
  
  //Get the spectra from the adjacent rapidity bins
  std::vector<SpectraClass *> adjacentSpectra;
  adjacentSpectra.push_back(spectraClass->at(0).at(centBin).at(midYIndex-1));
  adjacentSpectra.push_back(spectraClass->at(0).at(centBin).at(midYIndex+1));

  if (!adjacentSpectra.at(0) || !adjacentSpectra.at(1)){
    cout <<"WARNING - combineSpectra::HighEnergyMidRapidityException() - One or more of the adjacent spectra do not exist. Nothing done." <<endl;
    return;
  }

  std::vector<double> xVal, yVal, xErr, yErr;
  for (unsigned int iSpectrum=0; iSpectrum<adjacentSpectra.size(); iSpectrum++){

    //Get the Nearest Point
    int nP = FindElementWithMinDistance(adjacentSpectra.at(iSpectrum)->GetN(),
					adjacentSpectra.at(iSpectrum)->GetX(),
					mTm0);
    
    //Make sure the point is within this mTm0 bin
    if (fabs(adjacentSpectra.at(iSpectrum)->GetX()[nP] - mTm0) > mTm0BinWidth/2.0)
      continue;
    
    xVal.push_back(adjacentSpectra.at(iSpectrum)->GetX()[nP]);
    yVal.push_back(adjacentSpectra.at(iSpectrum)->GetY()[nP]);
    
    //The Weights are 1/err^2
    xErr.push_back(1.0 / pow(adjacentSpectra.at(iSpectrum)->GetEX()[nP],2));
    yErr.push_back(1.0 / pow(adjacentSpectra.at(iSpectrum)->GetEY()[nP],2));
    
  }//End Loop Over Spectra

  //Make sure there are two values
  if (xVal.size() != 2)
    return;

  //Calculate the Weighted Average, Weighted RMS, and Errors as before
  double avgX = TMath::Mean(xVal.size(),&xVal.at(0),&xErr.at(0));
  double avgY = TMath::Mean(yVal.size(),&yVal.at(0),&yErr.at(0));
  double rmsX = TMath::RMS(xVal.size(),&xVal.at(0),&xErr.at(0));
  double rmsY = TMath::RMS(yVal.size(),&yVal.at(0),&yErr.at(0));
  rmsX = rmsX/sqrt((double)xVal.size());
  rmsY = rmsY/sqrt((double)yVal.size());
  
  //Compute the Error on the Weighted Averages
  double errX(0), errY(0);
  for (unsigned int i=0; i<xVal.size(); i++){
    errX += xErr.at(i);
    errY += yErr.at(i);
  }
  errX = sqrt(1.0 / errX);
  errY = sqrt(1.0 / errY);
  
  //Compute the Total Error
  double totalErrX = sqrt(pow(rmsX,2) + pow(errX,2));
  double totalErrY = sqrt(pow(rmsY,2) + pow(errY,2));

  //Now Compute the Relative Error
  double relativeErrX = totalErrX / avgX;
  double relativeErrY = totalErrY / avgY;

  //Use the Relative Error to compute what the error should be
  //on the point in the spectrum of interest
  double currentErrX = currentSpectrum->GetX()[cP] * relativeErrX;
  double currentErrY = currentSpectrum->GetY()[cP] * relativeErrY;

  //Combine the Relative Error with the current Error and set the point value
  double newErrX = sqrt( pow(currentSpectrum->GetEX()[cP],2) + pow(currentErrX,2) );
  double newErrY = sqrt( pow(currentSpectrum->GetEY()[cP],2) + pow(currentErrY,2) );

  spectrum->SetPoint(spectrum->GetN(), currentSpectrum->GetX()[cP], currentSpectrum->GetY()[cP]);
  spectrum->SetPointError(spectrum->GetN()-1,newErrX,newErrY);

  return;
  
}
