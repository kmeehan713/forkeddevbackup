#include <iostream>
#include <vector>

#include <TFile.h>
#include <TMath.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TH3D.h>
#include <TF1.h>
#include <TCanvas.h>
#include <TSystem.h>
#include <TStyle.h>
#include <TGraphErrors.h>
#include <TGraphAsymmErrors.h>
#include <TVirtualFitter.h>
#include <TEfficiency.h>
#include <TMinuit.h>
#include <TRandom3.h>
#include <TFitResult.h>

#include "globalDefinitions.h"
#include "utilityFunctions.h"
#include "ParticleInfo.h"
#include "UserCuts.h"

#include "EfficiencyFitUtilities.h"

bool draw = false;

void MakeCorrectionCurves(TString embeddingFile, TString correctionFile, Int_t pid, Int_t charge){

  gStyle->SetOptFit(1);
  TVirtualFitter::SetMaxIterations(20000);
  
  //No spectra will go below mT-m0 = 0.1 so set min fit range
  const double minFitRange = 0.0; //mT-m0
  const int rebinVal = 5;
  ParticleInfo *particleInfo = new ParticleInfo();
  
  //Create the Output Correction File
  TFile *corrFile = new TFile(correctionFile,"UPDATE");
  corrFile->mkdir(Form("%s",particleInfo->GetParticleName(pid,charge).Data()));
  corrFile->cd(Form("%s",particleInfo->GetParticleName(pid,charge).Data()));
  gDirectory->mkdir("EfficiencyGraphs");
  gDirectory->mkdir("EfficiencyFits");
  gDirectory->mkdir("EnergyLossGraphs");
  gDirectory->mkdir("EnergyLossFits");

  //Open the Embedding File
  TFile *embFile = new TFile(embeddingFile,"READ");

  //Creat a canvas (optional)
  TCanvas *canvas = NULL;
  TCanvas *canvas1 = NULL;
  if (draw){
    canvas = new TCanvas("canvas","canvas",20,20,800,600);
    canvas1 = new TCanvas("canvas1","canvas1",20,20,800,600);
  }
  
  //Get the Number of Centrality Bins
  const int nCentBins = GetNCentralityBins();
  std::vector<double> centralityPercents = GetCentralityPercents();

  //Pointers to current 2D Efficiency and 3D Energy Loss histograms
  std::vector<TH2D *> embTrackHisto2D(nCentBins,(TH2D *)NULL);
  std::vector<TH2D *> matchTrackHisto2D(nCentBins,(TH2D *)NULL);
  std::vector<TH3D *> pTLossHisto3D(nCentBins,(TH3D *)NULL);

  //Centrality Integrated Histograms
  TH2D *embTrackHisto2DCentInt = NULL;
  TH2D *matchTrackHisto2DCentInt = NULL;
  TH3D *pTLossHisto3DCentInt = NULL;
  
  //Get the 2D and 3D Histograms from the file
  for (int iCentBin=0; iCentBin<nCentBins; iCentBin++){
    embTrackHisto2D.at(iCentBin) =
      (TH2D *)embFile->Get(Form("EfficiencyHistograms/embTrackHisto_Cent%d",iCentBin));
    matchTrackHisto2D.at(iCentBin) =
      (TH2D *)embFile->Get(Form("EfficiencyHistograms/matchTrackHisto_Cent%d",iCentBin));
    pTLossHisto3D.at(iCentBin) =
      (TH3D *)embFile->Get(Form("EnergyLossHistograms/pTLossHisto_Cent%d",iCentBin));

    if (iCentBin == 0){
      embTrackHisto2DCentInt = (TH2D *)embTrackHisto2D.at(iCentBin)->Clone("embTrackHisto_AllCent");
      matchTrackHisto2DCentInt = (TH2D *)matchTrackHisto2D.at(iCentBin)->Clone("matchTrackHisto_AllCent");
      pTLossHisto3DCentInt = (TH3D *)pTLossHisto3D.at(iCentBin)->Clone("pTLossHisto_AllCent");
    }
    else {
      embTrackHisto2DCentInt->Add(embTrackHisto2D.at(iCentBin));
      matchTrackHisto2DCentInt->Add(matchTrackHisto2D.at(iCentBin));
      pTLossHisto3DCentInt->Add(pTLossHisto3D.at(iCentBin));				  
    }
  }

  //*****************************************
  //   TPC TRACKING EFFICIENCY
  //*****************************************

  //----STEP ONE---
  //First Fit the centrality integrated graphs to extract the turn on
  //parameters of the error function. These parameters will be fixed
  //when we fit the efficiency graphs in each centrality bin
  std::vector<TF1 *> centIntEfficiencyFits(nRapidityBins,(TF1 *)NULL);
  Double_t prevPars[3] = {.7,0.005,2};
  for (int yIndex=0; yIndex<nRapidityBins; yIndex++){

    //Compute Kinematics
    Double_t rapidity = GetRapidityRangeCenter(yIndex);
    int yBinIndex = embTrackHisto2DCentInt->GetXaxis()->FindBin(rapidity);    
    
    //Get the 1D Histograms
    TH1D *embTrackHisto = embTrackHisto2DCentInt->ProjectionY("emby",yBinIndex,yBinIndex);
    TH1D *matchTrackHisto = matchTrackHisto2DCentInt->ProjectionY("matchy",yBinIndex,yBinIndex);
    
    if (embTrackHisto->GetEntries() < 10 || matchTrackHisto->GetEntries() < 10){
      delete embTrackHisto;
      delete matchTrackHisto;
      continue;
    }

    //Create the Efficiency Graph
    corrFile->cd();
    TGraphAsymmErrors *efficiencyGraph = new TGraphAsymmErrors();
    efficiencyGraph->BayesDivide(matchTrackHisto,embTrackHisto);
    

    //Create the Fit
    centIntEfficiencyFits.at(yIndex) = new TF1(Form("tpcEfficiencyFit_%s_AllCent_yIndex%d",
						    particleInfo->GetParticleName(pid,charge).Data(),
						    yIndex),
					       "[0]*exp(-([1]/pow(x,[2])))",minFitRange,2.0);
    centIntEfficiencyFits.at(yIndex)->SetNpx(10000);
    centIntEfficiencyFits.at(yIndex)->SetLineWidth(3);
    centIntEfficiencyFits.at(yIndex)->SetLineColor(particleInfo->GetParticleColor(pid));
    centIntEfficiencyFits.at(yIndex)->SetLineStyle(7);
    
    centIntEfficiencyFits.at(yIndex)->SetParameters(prevPars);
    centIntEfficiencyFits.at(yIndex)->SetParLimits(0,0.005,0.9);
    centIntEfficiencyFits.at(yIndex)->SetParLimits(1,0.000001,.07);
    centIntEfficiencyFits.at(yIndex)->SetParLimits(2,.1,25);
    
    TFitResultPtr fitResult = efficiencyGraph->Fit(centIntEfficiencyFits.at(yIndex),"RNSEX0");
    int attempt(0);
    while (fitResult.Get()->Status() != 0 && attempt < 10) {
      fitResult = efficiencyGraph->Fit(centIntEfficiencyFits.at(yIndex),"RNSEX0");
      cout <<attempt++ <<endl;;
    }

    prevPars[0] = centIntEfficiencyFits.at(yIndex)->GetParameter(0);
    prevPars[1] = centIntEfficiencyFits.at(yIndex)->GetParameter(1);
    prevPars[2] = centIntEfficiencyFits.at(yIndex)->GetParameter(2);
    
    if (draw){
      canvas->cd();
      TH1F *frame = canvas->DrawFrame(0,0,2.2,1.2);
      frame->SetTitle(efficiencyGraph->GetTitle());
      efficiencyGraph->Draw("PZ");
      centIntEfficiencyFits.at(yIndex)->Draw("SAME");
      canvas->Update();
      gSystem->Sleep(1000);
    }

    //Clean Up
    delete efficiencyGraph;
    
  }//End Loop Over yIndex to fit centrality integrated efficiencies

  //----STEP TWO---
  //Loop Over the centrality bins, for each cent bin construct the 1D efficiency and
  //energy loss graphs for various rapidity bins. Use the fits to the centrality integrated
  //graphs from STEP ONE to fix the turn on parameters.
  for (int iCentBin=0; iCentBin<nCentBins; iCentBin++){

    //Loop Over the Rapidity Bins
    for (int yIndex=0; yIndex<nRapidityBins; yIndex++){

      //Compute Kinematics
      Double_t rapidity = GetRapidityRangeCenter(yIndex);
      int yBinIndex = embTrackHisto2D.at(iCentBin)->GetXaxis()->FindBin(rapidity);

 
      //Get the 1D Histograms
      TH1D *embTrackHisto = embTrackHisto2D.at(iCentBin)->ProjectionY("emby",yBinIndex,yBinIndex);
      TH1D *matchTrackHisto = matchTrackHisto2D.at(iCentBin)->ProjectionY("matchy",yBinIndex,yBinIndex);

      if (embTrackHisto->GetEntries() < 10 || matchTrackHisto->GetEntries() < 10)
	continue;

      //Rebin
      embTrackHisto->Rebin(rebinVal);
      matchTrackHisto->Rebin(rebinVal);
      
      //Create the Efficiency Graph
      corrFile->cd();

      TGraphAsymmErrors *efficiencyGraph = new TGraphAsymmErrors();
      efficiencyGraph->BayesDivide(matchTrackHisto,embTrackHisto);
      
      efficiencyGraph->SetMarkerStyle(particleInfo->GetParticleMarker(pid,charge));
      efficiencyGraph->SetMarkerColor(particleInfo->GetParticleColor(pid));
      efficiencyGraph->SetName(Form("tpcEfficiencyGraph_%s_Cent%d_yIndex%d",
				    particleInfo->GetParticleName(pid,charge).Data(),
				    iCentBin,yIndex));
      efficiencyGraph->SetTitle(Form("TPC Tracking Efficiency %s | Cent=[%.02g,%.02g]%% | y_{%s}^{Emb}=[%.03g,%.03g];(m_{T}-m_{%s})^{Emb};Efficiency #times Acceptance",
				    particleInfo->GetParticleSymbol(pid,charge).Data(),
				    iCentBin == nCentBins-1 ? 0:centralityPercents.at(iCentBin+1),
				    centralityPercents.at(iCentBin),
				    particleInfo->GetParticleSymbol(pid).Data(),
				    GetRapidityRangeLow(yIndex),
				    GetRapidityRangeHigh(yIndex),
				    particleInfo->GetParticleSymbol(pid).Data()));
      efficiencyGraph->SetMarkerStyle(particleInfo->GetParticleMarker(pid,charge));
      efficiencyGraph->SetMarkerColor(particleInfo->GetParticleColor(pid));
      efficiencyGraph->SetMarkerSize(pid == KAON ? 1.5 : 1.2);

      
      //Create the Fit
      TF1 efficiencyFit(Form("tpcEfficiencyFit_%s_Cent%d_yIndex%d",
			     particleInfo->GetParticleName(pid,charge).Data(),
			     iCentBin,yIndex),
			"[0]*exp(-([1]/pow(x,[2])))",minFitRange,2.0);
      efficiencyFit.SetNpx(10000);
      efficiencyFit.SetLineWidth(3);
      efficiencyFit.SetLineColor(particleInfo->GetParticleColor(pid));
      efficiencyFit.SetLineStyle(1);

      efficiencyFit.SetParameter(0,centIntEfficiencyFits.at(yIndex)->GetParameter(0));
      efficiencyFit.SetParameter(1,centIntEfficiencyFits.at(yIndex)->GetParameter(1));
      efficiencyFit.SetParameter(2,centIntEfficiencyFits.at(yIndex)->GetParameter(2));

      efficiencyFit.SetParLimits(0,.2,1);
      efficiencyFit.SetParLimits(1,0.00001,.25);
      efficiencyFit.SetParLimits(2,1.0,6.0);

      //Create the Fit which will be used for the systematic Errors
      //This function is the same form as the nominal fit above,
      //but will not have its parameters fixed.
      TF1 efficiencyFitSys(Form("%s_Sys",efficiencyFit.GetName()),
			   Form("%s",efficiencyFit.GetExpFormula().Data()),
			   minFitRange,2.0);
      efficiencyFitSys.SetNpx(10000);
      efficiencyFitSys.SetLineWidth(3);
      efficiencyFitSys.SetLineColor(kGray);
      efficiencyFitSys.SetLineStyle(7);
      
      efficiencyFitSys.SetParameter(0,efficiencyFit.GetParameter(0));
      efficiencyFitSys.FixParameter(1,efficiencyFit.GetParameter(1));
      efficiencyFitSys.SetParameter(2,efficiencyFit.GetParameter(2));

      efficiencyFitSys.SetParLimits(0,efficiencyFitSys.GetParameter(0)*.5,1.0);
      efficiencyFit.SetParLimits(1,0.00001,.25);
      //efficiencyFitSys.SetParLimits(1,TMath::Min(0.000001,efficiencyFitSys.GetParameter(1)),fabs(efficiencyFitSys.GetParameter(1))*5.0);
      efficiencyFitSys.SetParLimits(2,1.0,6.0);
				    
      
      TFitResultPtr fitResult = efficiencyGraph->Fit(&efficiencyFit,"RS");
      int attempt(0);
      while (fitResult.Get()->Status() != 0 && attempt < 10 ||
	     (efficiencyFit.GetParameter(1) - efficiencyFit.GetParError(1)) < 0) {
	cout <<"*********REFITTING!*********" <<endl;

	if (efficiencyFit.GetParameter(1) - efficiencyFit.GetParError(1) < 0){
	  efficiencyFit.FixParameter(1,efficiencyFit.GetParameter(1));
	}
	
	efficiencyFit.SetParameter(0,centIntEfficiencyFits.at(yIndex)->GetParameter(0));
	efficiencyFit.SetParameter(2,centIntEfficiencyFits.at(yIndex)->GetParameter(2));
	
	fitResult = efficiencyGraph->Fit(&efficiencyFit,"RS");
	attempt++;
	
	cout <<attempt <<" " <<fitResult.Get()->Status() <<endl;
      }
      
      prevPars[0] = efficiencyFit.GetParameter(0);
      prevPars[1] = efficiencyFit.GetParameter(1);
      prevPars[2] = efficiencyFit.GetParameter(2);

      efficiencyFit.SetParError(1,centIntEfficiencyFits.at(yIndex)->GetParError(1));
      efficiencyFit.SetParError(2,centIntEfficiencyFits.at(yIndex)->GetParError(2));

      TGraphErrors *efficiencyFitConf =
	GetConfidenceIntervalOfFit(&efficiencyFit);

      //Fit the Systematic Function
      efficiencyGraph->Fit(&efficiencyFitSys,"REX0+");
      TGraphErrors *efficiencyFitSysConf =
	GetConfidenceIntervalOfFit(&efficiencyFitSys);
      
      if (draw){
	canvas->cd();
	TH1F *frame = canvas->DrawFrame(0,0,2.2,1.2);
	frame->SetTitle(efficiencyGraph->GetTitle());
	//confInterval->Draw("2");
	efficiencyGraph->Draw("PZ");
	centIntEfficiencyFits.at(yIndex)->Draw("SAME");
	canvas->Update();
	gSystem->Sleep(1000);
      }
      
      //Save
      corrFile->cd();
      corrFile->cd(Form("%s",particleInfo->GetParticleName(pid,charge).Data()));
      gDirectory->cd("EfficiencyGraphs");
      efficiencyGraph->Write(efficiencyGraph->GetName(),TObject::kOverwrite);
      corrFile->cd();
      corrFile->cd(Form("%s",particleInfo->GetParticleName(pid,charge).Data()));
      gDirectory->cd("EfficiencyFits");
      efficiencyFit.Write(efficiencyFit.GetName(),TObject::kOverwrite);
      efficiencyFitConf->Write(efficiencyFitConf->GetName(),TObject::kOverwrite);
      efficiencyFitSys.Write(efficiencyFitSys.GetName(),TObject::kOverwrite);
      efficiencyFitSysConf->Write(efficiencyFitSysConf->GetName(),TObject::kOverwrite);
      corrFile->cd();

      //Clean Up
      if (efficiencyGraph)
	delete efficiencyGraph;
      if (efficiencyFitConf)
	delete efficiencyFitConf;
      if (efficiencyFitSysConf)
	delete efficiencyFitSysConf;


    }//End Loop Over yIndex

  }//End Loop Over Centrality Bin

  
  //*****************************************
  //   ENERGY LOSS
  //*****************************************
  //Loop Over the Rapidity Bins
  for (int yIndex=0; yIndex<nRapidityBins; yIndex++){

    TGraphErrors pTLossGraph;
    pTLossGraph.SetName(Form("energyLossGraph_%s_yIndex%d",
			     particleInfo->GetParticleName(pid,charge).Data(),yIndex));
    pTLossGraph.SetTitle(Form("Energy Loss %s | y_{%s}=[%.03g,%.03g];p_{T}^{Reco};p_{T}^{Reco}-p_{T}^{Emb}",
			      particleInfo->GetParticleSymbol(pid,charge).Data(),
			      particleInfo->GetParticleSymbol(pid).Data(),
			      GetRapidityRangeLow(yIndex),
			      GetRapidityRangeHigh(yIndex),
			      particleInfo->GetParticleSymbol(pid).Data()));
    pTLossGraph.SetMarkerStyle(particleInfo->GetParticleMarker(pid,charge));
    pTLossGraph.SetMarkerColor(particleInfo->GetParticleColor(pid));
    
    TH1D *htemp = NULL;
    for (int pTIndex=1; pTIndex<pTLossHisto3DCentInt->GetYaxis()->GetNbins(); pTIndex++){

      //Compute Kinematics
      Double_t rapidity = GetRapidityRangeCenter(yIndex);
      int yBinIndex = pTLossHisto3DCentInt->GetXaxis()->FindBin(rapidity);
      Double_t pTReco = pTLossHisto3DCentInt->GetYaxis()->GetBinCenter(pTIndex);
      
      htemp = pTLossHisto3DCentInt->ProjectionZ("",yBinIndex,yBinIndex,pTIndex,pTIndex);
      
      //Skip if there are too few entries
      if (htemp->GetEntries() < 2){
	delete htemp;
	continue;
      }

      pTLossGraph.SetPoint(pTLossGraph.GetN(),pTReco,htemp->GetMean());
      pTLossGraph.SetPointError(pTLossGraph.GetN()-1,0,
				htemp->GetRMS()/(Double_t)sqrt(htemp->GetEntries()));
      
      if (draw){
	canvas1->cd();
	htemp->Draw();
	canvas1->Update();
	gSystem->Sleep(2000);
      }
      
      delete htemp;
      
    }//End loop over bins


    //Skip Graphs with too few entries
    if (pTLossGraph.GetN() < 5)
      continue;

    //Get the Fit Range
    Double_t fitRangeLow  = pTLossGraph.GetX()[0]-.1;
    Double_t fitRangeHigh = pTLossGraph.GetX()[pTLossGraph.GetN()-1]+.1;
    
    //The Pions are Fit with a constant since their energy loss is already corrected
    //by the tracking software, but there may be an overall offset
    TF1 *energyLossFit = NULL;
    TGraphErrors *energyLossFitConf = NULL;
    if (pid == PION){

      energyLossFit = new TF1(Form("energyLossFit_%s_yIndex%d",
				   particleInfo->GetParticleName(pid,charge).Data(),
				   yIndex),
			      "pol0",fitRangeLow,fitRangeHigh);
    }

    //Other particles are fit with a more complicated function
    else{
      energyLossFit = new TF1(Form("energyLossFit_%s_yIndex%d",
				   particleInfo->GetParticleName(pid,charge).Data(),
				   yIndex),
			      "[0]+[1]*TMath::Power(x,-[2])",fitRangeLow,fitRangeHigh);
      energyLossFit->SetParameter(0,.001);
      energyLossFit->SetParameter(1,-.01);
      energyLossFit->SetParameter(2,1.0);

      energyLossFit->SetParLimits(0,0,.05);
      energyLossFit->SetParLimits(1,-.05,0.0);
      energyLossFit->SetParLimits(2,.5,10);

    }

    energyLossFit->SetNpx(10000);
    energyLossFit->SetLineWidth(3);
    energyLossFit->SetLineColor(kBlack);
    energyLossFit->SetLineStyle(7);

    pTLossGraph.Fit(energyLossFit,"R");
    energyLossFitConf = GetConfidenceIntervalOfFit(energyLossFit);  
 
    if (draw){
      canvas1->cd();
      pTLossGraph.Draw("AP");
      canvas1->Update();
      gSystem->Sleep(500);
    }
    
    //Save
    corrFile->cd();
    corrFile->cd(Form("%s",particleInfo->GetParticleName(pid,charge).Data()));
    gDirectory->cd("EnergyLossGraphs");
    pTLossGraph.Write(pTLossGraph.GetName(),TObject::kOverwrite);
    corrFile->cd();
    corrFile->cd(Form("%s",particleInfo->GetParticleName(pid,charge).Data()));
    gDirectory->cd("EnergyLossFits");
    energyLossFit->Write(energyLossFit->GetName(),TObject::kOverwrite);
    energyLossFitConf->Write(energyLossFitConf->GetName(),TObject::kOverwrite);
    corrFile->cd();

    delete energyLossFit;
    delete energyLossFitConf;
  }//End Loop Over yIndex
    
}//End Main


