#include "TFile.h"
#include "TGraphAsymmErrors.h"
#include "TF1.h"
#include "TCanvas.h"

#include <iostream>
#include <fstream>

#include "globalDefinitions.h"
#include "utilityFunctions.h"
#include "ParticleInfo.h"


using namespace std;

//___________________________________________________________________________
// The purpose of this code is to read root files of fitted efficiency curves 
// and generate a table of efficiency values and their uncertainties as a 
// function of mT-m0 for each rapidity and centrality bin
//___________________________________________________________________________

void CreateEfficiencyTable(TString effFileName, TString outFileName, Int_t pid, Int_t charge, Int_t nCentBins, Int_t nRapBins){

  //Set # of rapidity and centrality bins
	ParticleInfo *particleInfo = new ParticleInfo();
		
	//create the output text file
  ofstream outFile (outFileName, ofstream::out);

	//read input root file
	TFile *effFile = new TFile(effFileName, "READ");

  TGraphAsymmErrors *effGraph = NULL;
  TF1* fEff;
  Int_t nPoints;

	//loop over centralities and rapidities to obtain each TGraph/TF1
	for (Int_t centIndex = 0; centIndex < nCentBins; centIndex++){

	  for (Int_t rapIndex = 0; rapIndex < nRapBins; rapIndex++){

      //Obtain efficiency graph and fit
			effGraph = (TGraphAsymmErrors*) effFile->Get(Form("%s/EfficiencyGraphs/tpcEfficiencyGraph_%s_Cent%d_yIndex%d",particleInfo->GetParticleName(pid,charge).Data(),particleInfo->GetParticleName(pid,charge).Data(),centIndex,rapIndex));
      //protect against graphs that weren't created due to acceptance
			if (effGraph == NULL) continue;
      fEff = effGraph->GetFunction(Form("tpcEfficiencyFit_%s_Cent%d_yIndex%d",particleInfo->GetParticleName(pid,charge).Data(),centIndex,rapIndex));

      //loop over mT-m0 bins in graph and evaluate fit function at bin center and do error propagation
      nPoints = effGraph->GetN();
      double x[nPoints], y[nPoints], yEff;
      for (Int_t mtm0Index = 0; mtm0Index<nPoints; mtm0Index++){

      	effGraph->GetPoint(mtm0Index, x[mtm0Index], y[mtm0Index]);
        yEff = fEff->Eval(x[mtm0Index]);

        outFile<<centIndex<<"   "<<rapIndex<<"   "<<x[mtm0Index]<<"   "<<yEff<<endl;

      }

      effGraph->Clear();
      fEff->Clear();

		}//End of loop over rapidities

	}//End of loop over centralities

} //end of CreateEfficiencyTable.C
