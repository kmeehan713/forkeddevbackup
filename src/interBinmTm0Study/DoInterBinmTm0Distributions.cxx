#include <iostream>
#include <utility>
#include <vector>

#include <TString.h>
#include <TFile.h>
#include <TTree.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TH3D.h>
#include <TMath.h>
#include <TF1.h>
#include <TGraphErrors.h>
#include <TSystem.h>
#include <TLine.h>
#include <TObject.h>


#include "globalDefinitions.h"
#include "utilityFunctions.h"
#include "TrackInfo.h"
#include "PrimaryVertexInfo.h"
#include "EventInfo.h"
#include "DavisDstReader.h"
#include "ParticleInfo.h"
#include "UserCuts.h"
#include "fitZTPCUtilities.h"


//_______________________________________________________________
void DoInterBinmTm0Distributions(TString interBinmTm0File, TString correctionFile){

  //Open the File containing the mTm0Histograms
  TFile *mTm0File = new TFile(interBinmTm0File,"READ");

  //Open the Correction File for writing
  TFile *corrFile = new TFile(correctionFile,"UPDATE");
  
  //Set the particle vector
  std::vector<int> particles;
  particles.push_back(PION);
  particles.push_back(KAON);
  particles.push_back(PROTON);

  //Set the Charge Vector
  std::vector<int> charges;
  charges.push_back(-1);
  charges.push_back(1);
  
  const unsigned int nCentralityBins = GetNCentralityBins();
  const unsigned int nParticles      = particles.size();
  const unsigned int nCharges        = charges.size();
  std::vector<double> centralityPercents = GetCentralityPercents();

  //Create a Particle Info Object
  ParticleInfo *particleInfo = new ParticleInfo();
  
  //Load the 3D mTm0 Histograms
  std::vector< std::vector < std::vector <TH3D *> > > mTm0Histos
    (nParticles, std::vector < std::vector <TH3D *> >
     (nCharges, std::vector <TH3D *>
      (nCentralityBins, (TH3D *)NULL)));
  for (unsigned int iParticle=0; iParticle<nParticles; iParticle++){
    for (unsigned int iCharge=0; iCharge<nCharges; iCharge++){

      //Make new directories for the fits
      corrFile->mkdir(Form("%s/InterBinmTm0Histos/",
			   particleInfo->GetParticleName(particles.at(iParticle),
							 charges.at(iCharge)).Data()));
      corrFile->mkdir(Form("%s/InterBinmTm0Fits/",
			   particleInfo->GetParticleName(particles.at(iParticle),
							 charges.at(iCharge)).Data()));
      
      for (unsigned int iCentBin=0; iCentBin<nCentralityBins; iCentBin++){
	
	mTm0Histos.at(iParticle).at(iCharge).at(iCentBin) =
	  (TH3D *)mTm0File->Get(Form("%s/InterBinmTm0Histos/mTm0Histo_%s_Cent%02d",
				     particleInfo->GetParticleName(particles.at(iParticle),charges.at(iCharge)).Data(),
				     particleInfo->GetParticleName(particles.at(iParticle),charges.at(iCharge)).Data(),
				     iCentBin));	
	
	corrFile->cd();
	corrFile->cd(Form("%s/InterBinmTm0Histos/",
			  particleInfo->GetParticleName(iParticle,charges.at(iCharge)).Data()));
	mTm0Histos.at(iParticle).at(iCharge).at(iCentBin)->
	  Write(mTm0Histos.at(iParticle).at(iCharge).at(iCentBin)->GetName(),TObject::kOverwrite);

      }//End Loop Over Centrality Bins
    }//End Loop Over Charges
  }//End Loop Over Particles

  //Create a static function that will server to illustrate the bin center
  TF1 *line = new TF1("binCenter","pol0",0.0,2.0);
  line->FixParameter(0,0.01250);
  line->SetLineWidth(3);
  line->SetLineStyle(9);
  line->SetLineColor(kGray+2);

  //Create a function to fit the graphs with
  TF1 *fit = new TF1("","pol0",0.0,2.0);
  fit->SetParameter(0,0.01250);
  fit->SetLineWidth(3);
  fit->SetLineStyle(1);

  //Create Pointer to a TGraphErrors to hold the Confidence Interval
  TGraphErrors *fitConf = NULL;
    
  std::vector < std::vector < std::vector < std::vector <TGraphErrors *> > > > avgmTm0Graph
    (nParticles, std::vector < std::vector <std::vector <TGraphErrors *> > >
     (nCharges, std::vector < std::vector <TGraphErrors *> >
      (nCentralityBins, std::vector <TGraphErrors *>
       (nRapidityBins, (TGraphErrors *)NULL))));
  for (unsigned int iParticle=0; iParticle<nParticles; iParticle++){
    for (unsigned int iCharge=0; iCharge<nCharges; iCharge++){
      for (unsigned int iCentBin=0; iCentBin<nCentralityBins; iCentBin++){
	for (int yIndex=0; yIndex<nRapidityBins; yIndex++){
	  
	  
	  //Create the Graph for this rapidity bin
	  avgmTm0Graph.at(iParticle).at(iCharge).at(iCentBin).at(yIndex) = new TGraphErrors();
	  TGraphErrors *graph = avgmTm0Graph.at(iParticle).at(iCharge).at(iCentBin).at(yIndex);
	  
	  graph->SetName(Form("avgmTm0_%s_Cent%02d_yIndex%02d",
			      particleInfo->GetParticleName(iParticle,charges.at(iCharge)).Data(),
			      iCentBin,yIndex));
	  graph->SetTitle(Form("Average Transverse Mass | PID: %s | Cent=[%.02g,%.02g]%% | y_{%s}=[%.03g,%.03g];m_{T}-m_{%s} (GeV/c^{2});#LT(m_{T}-m_{%s})_{Measured}-(m_{T}-m_{%s})_{Low Bin Edge}#GT (GeV/c^{2})",
			       particleInfo->GetParticleSymbol(iParticle,charges.at(iCharge)).Data(),
			       iCentBin == nCentralityBins-1 ? 0:centralityPercents.at(iCentBin+1),
			       centralityPercents.at(iCentBin),
			       particleInfo->GetParticleSymbol(iParticle,charges.at(iCharge)).Data(),
			       GetRapidityRangeLow(yIndex),GetRapidityRangeHigh(yIndex),
			       particleInfo->GetParticleSymbol(iParticle,charges.at(iCharge)).Data(),
			       particleInfo->GetParticleSymbol(iParticle,charges.at(iCharge)).Data(),
			       particleInfo->GetParticleSymbol(iParticle,charges.at(iCharge)).Data()));
	  graph->SetMarkerStyle(particleInfo->GetParticleMarker(particles.at(iParticle),charges.at(iCharge)));
	  graph->SetMarkerColor(particleInfo->GetParticleColor(particles.at(iParticle)));
	  graph->GetYaxis()->SetRangeUser(0.0,0.025);
	  graph->GetXaxis()->SetRangeUser(0.0,2.0);
	  
	  fit->SetName(Form("%s_Fit",graph->GetName()));
	  fit->SetLineColor(particleInfo->GetParticleColor(particles.at(iParticle)));
	  
	  for(int mTm0Bin=1; mTm0Bin<mTm0Histos.at(iParticle).at(iCharge).at(iCentBin)->GetNbinsY(); mTm0Bin++){
	    
	    //Compute the Rapidity For this bin
	    Double_t rapidity = GetRapidityRangeCenter(yIndex);
	    Double_t mTm0     = mTm0Histos.at(iParticle).at(iCharge).at(iCentBin)->GetYaxis()->GetBinCenter(mTm0Bin);
	    
	    //Skip mTm0 Bins that are less that .1 GeV since we don't use them in the spectra
	    if (mTm0 < 0.1)
	      continue;
	    
	    //Get a 1D Histogram from the 3D histogram
	    //The Get Yield Histogram simply return a projection along z so it works here
	    TH1D *temp = GetYieldHistogram(mTm0Histos.at(iParticle).at(iCharge).at(iCentBin),rapidity,mTm0,"");
	    
	    //Check that the Histogram has enough entries
	    if (temp->GetEntries() < 10)
	      continue;
	    
	    //Get the Mean of the Histogram
	    Double_t mean = temp->GetMean();
	    
	    graph->SetPoint(graph->GetN(),mTm0,mean);

	    //For an exponential distribution the standard deviation is equal to the mean
	    //so the standard error is then
	    Double_t stdErr = mean / (double)temp->GetEntries();
	    
	    //The inter bin mTm0 distribution is an exponential, so compute
	    //the 95% confidence interval of the mean of the exponential (see wiki page)
	    //Double_t confInterval95 = mean * ( 1 + 1.96/TMath::Sqrt(temp->GetEntries()));
	    //confInterval95 -= mean;
	    
	    graph->SetPointError(graph->GetN()-1,mTm0BinWidth/2.0,stdErr);
	    
	    if (temp){
	      delete temp;
	      temp = NULL;
	    }
	    
	  }//End Loop Over mTm0 Bins of Histogram
	  
	  if (graph->GetN() < 2)
	    continue;
	  
	  //Fit the Graph
	  graph->Fit(fit,"REX0Q+");
	  fitConf = GetConfidenceIntervalOfFit(fit);	
	  
	  //Add the Bin Center Line to the Graph
	  ((TList *)graph->GetListOfFunctions())->Add((TObject *)line);
	  
	  //Save the Graph
	  corrFile->cd(Form("%s/InterBinmTm0Histos/",
			    particleInfo->GetParticleName(iParticle,charges.at(iCharge)).Data()));
	  graph->Write(graph->GetName(),TObject::kOverwrite);
	  
	  corrFile->cd(Form("%s/InterBinmTm0Fits/",
			    particleInfo->GetParticleName(particles.at(iParticle),
							  charges.at(iCharge)).Data()));
	  fit->Write(fit->GetName(),TObject::kOverwrite);
	  fitConf->Write(fitConf->GetName(),TObject::kOverwrite);
	  
	  mTm0File->cd();
	  
	}//End Loop Over Rapidity Bins
      }//End Loop Over Centrality Bins
    }//End Loop Over Charges
  }//End Loop Over Particles
  


}
