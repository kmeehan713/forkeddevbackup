This is the README file for the scripts directory.

In this directory there are bash scripts that perform analysis operations.

#############
## Data QA ##
#############

OVERVIEW:
Before you can run an analysis, you have to Quality Assure your data.  In this directory you will find bash scripts that, when executed, do just that.  Before doing 
this you should check your UserCuts file, to make sure all of your desired cuts are included.

LIST OF SCRIPTS:
     RunEventQA.sh:
     RunTrackQA.sh
     RunVertexQA.sh

THINGS TO CHECK IN SCRIPTS:
     All of the settings lie at the beginning of these scripts. The code is well commmented, but especially make sure you have the following lines set appropriately.

     Line10: Change to the path to the directory where you have your data stored.
     Line13: Change to the path to the directory where you would like your output stored.
     Line16: Make sure this is set to -1 in order to run over all of the events.

###################
## Data Analysis ##
###################

----------------------------------
## Centrality Cut Determination ##
----------------------------------
     OVERVIEW:
     We would like to determine a centrality cut for our data.  We only want the most central events because these events have the highest chance of interaction, and 
     thus interesting physics.  We are not omniscient though, so there is no way to tell directly from the data which events are most central.  But usually there is a 
     correlation between a variable (number of Pions or number of Good Tracks) and how central the collision from which they came was.  Our goal is to accurately 
     "guess" what cut on these variables would give us top centrality events.  This is done with a Glauber model.  The Glauber model simulates a heavy-ion collision, 
     and produces the likely observables we would see.  Therefore, we have the distributions we see in data, and also have the corresponding "inaccessible" information 
     (like centrality).  By fitting the simulated data distributions to the real data we can then extract the likely centrality information.

     LIST OF SCRIPTS:
     RunGlauberSimulation.sh
     RunCentralityVariableDistributions.sh
     RunCentralityDetermination.sh

     THINGS TO CHECK IN SCRIPTS:
          RunGlauberSimulation.sh - 
	       Runs the Glauber model simulation.  Produces a root file containing all observable and unobservable quantities calculated in the Glauber model. The 
	       code is well commented.  Below are the things you will probably need to change.

	       Line 7: Number of nucleons in one of the colliding nuclei (Au = 197 | Al = 27).
	       Line 8: Number of nucleons in one of the colliding nuclei (Au = 197 | Al = 27).
	       Line 12: Set this to the path to the directory where you would like the output stored.

          RunCentralityVariableDistributions.sh -
               Runs over the data and creates histograms of the observables to which you will compare the Glauber model. 
	       The code is well commented.  Below are the things you will probably need to change.

	       Line10: Change to the path to the directory where you have your data stored.
	       Line13: Change to the path to the directory where you would like your output stored.
	       Line16: Make sure this is set to -1 in order to run over all of the events.
	  
	  RunCentralityDetermination.sh - 
	       Compares the Glauber model simulation results to the data histograms, and extracts the cut on the observable corresponding to a certain event centrality 
	       and calculates all relevant systematic errors.  It prints to the screen a table summarizing these results.  Make sure you save this output. The code is 
	       well commented.  Below are the things you will probably need to change.

	       Line 7: This is the path and name of the file produced by RunCentralityVariableDistributions.sh. 
	       Line 8: This is the name of the histogram of the observable within the .root file produced by RunCentralityVariableDistributions.sh.
	       Line 9: This is the path and name of the file produced by RunGlauberSimulation.sh.
	       Line 10: This is the path and name of the outputfile
	       Line 11 and 12:  These are set to avoid detector inefficiencies (low end) and pileup (high end) within the data histograms throwing off the Glauber fit.

-----------------------------------------------------------------
## Centrality Cut Application And Data Preparation for Spectra ##
-----------------------------------------------------------------

	OVERVIEW:
	In this step, we need to apply our centrality (and other) cuts to the data and produce the dE/dx distributions, from which we will obtain the Yield Histograms 
	that plot the zTPC distributions of particle species (Pions, Kaons, and Protons) as a function of mT-m0, centrality, and rapidity.  For easy storage, the 
	produced file contains 3 dimensional histograms of zTPC, mT-m0, and rapidity for each species.  With simple cuts on this object you can create the useful 1 
	dimensional histograms.

	LIST OF SCRIPTS:
	RunSkimmerAndBinner.bash 

	THINGS TO CHECK IN SCRIPT: 

-----------------	       
## Raw Spectra ##
-----------------	       

	OVERVIEW:
	We want to create particle spectra for each species, at each rapidity, as a function of mT-m0.  This is done by fitting the yield histograms with gaussians to 
	extract a functional form for a given particle.  The integral of this function for a specific rapidity and mT-m0 value yields a point on the spectra.  This 
	process is repeated for all mT-m0 values to create the full spectra of that particle, at that rapidity.  RunFitZTPCPions.sh directs the code to extract the 
	functional form of the particle from the Yield histograms, and, using this, also creates the spectra.  Eventually there will be code to create the spectra for 
	kaons and protons, too.  For now there is only pion spectra code.

	LIST OF SCRIPTS:
	RunFitZTPCPions.sh

	THINGS TO CHECK IN SCRIPT: 

------------------------
## Corrected Spectra  ##
------------------------

------------------------
## dN/dy Distribution ##
------------------------


	