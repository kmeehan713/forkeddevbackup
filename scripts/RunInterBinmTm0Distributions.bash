#!/bin/bash

help_func(){

    echo ""
    echo "HELP INFORMATION"
    echo "    NAME:"
    echo "        RunInterBinmTm0Distributions.bash"
    echo "    PURPOSE:"
    echo "        This creates histograms containing the mTm0 values of identified"
    echo "        particles in each analysis bin so that the mTm0 distributions can"
    echo "        be used to set the mTm0 point of the spectra."
    echo "    USAGE:"
    echo "        ./RunInterBinmTm0Distributions.bash [OPTION] ... [CONFIG FILE]"
    echo "    REQUIRED ARGUMENTS:"
    echo "        configuration file - name of configuration file with path if applicable"
    echo "    OPTIONS:"
    echo "        -h - display this help information"
    echo "        -a - only do the analysis section - e.g. use the already existing interBinmTm0 file"
    echo ""
    echo ""
}

analysisOnly=0

#Check for options
while [ "$#" -gt 0 ]; do
    while getopts "a:h" opts; do
	case "$opts" in
	    a) analysisOnly="${OPTARG}"; shift;;
	    h) help_func; exit 1;;
	    ?) exit 1;;
	    *) echo "For help use option: -h"; exit 1;;
	esac
	shift
	OPTIND=1
    done

    if [ "$#" -gt 0 ]; then
	POSITIONALPARAM=(${POSITIONALPARAM[@]} $1)
	shift
	OPTIND=1
    fi
done

#Make sure only one argument remains after all the options
if [ "${#POSITIONALPARAM[@]}" -ne 1 ]; then
    echo "ERROR: This script requires only one argument. For usage information use option -h."
    echo "       If you have used options check your formating."
    exit 1
fi

#Assign user inputs to variables
configFile=${POSITIONALPARAM[0]}

#Make Sure the configuration variable is set
if [ -z $configFile ]; then
    echo "ERROR: Configuration file was not set. For usage information use option -h"
    exit 1
fi

#Make Sure the configuration file exists
if [ ! -e $configFile ]; then
    echo "ERROR: Configruation file ($configFile) was not found!"
    exit 1
fi

#Get the Configuration from the Config file
source $configFile

if [ $analysisOnly -eq 0 ]; then 
    
    #Variables used below
    processID=()
    outFiles=()
    
    for i in ${tofMatchInput[@]}; do
	
	rndStr=$(cat /dev/urandom | tr -dc 'A-Z0-9' | fold -w 12 | head -n 1)
	tempOutFile=$tmpDir/$rndStr.root
	outFiles+=($tempOutFile)
	
	nice root -l -b -q ../macros/RunMakeInterBinmTm0Distributions.C\(\"$i\",\"$tempOutFile\",\"$correctionFile\",\"$tofMatchEffFile\",\"$starLib\",$energy,\"$eventConfig\"\) > $logDir/MakeInterBinmTm0Dists\_$rndStr.log 2>&1 &
	
	processID+=($!)
	
    done
    wait ${processID[@]}
    
    hadd -f $interBinmTm0File ${outFiles[@]}
    
    rm ${outFiles[@]}
    
fi

if [ ! -e $interBinmTm0File ]; then
    echo "The interBinmTm0 Histogram File does not exist! You must first create it by running this script with no arguments."
    exit 0
fi

nice root -l -b -q ../macros/RunDoInterBinmTm0DistributionStudy.C\(\"$interBinmTm0File\",\"$correctionFile\",\"$starLib\",$energy,\"$eventConfig\"\)

exit 0
