#!/bin/bash

help_func(){

    echo ""
    echo "HELP INFORMATION"
    echo "    NAME:"
    echo "        RunBindEdxInRapidityMtM0.bash"
    echo "    PURPOSE:"
    echo "        This is the first step in the pid calibration."
    echo "    USAGE:"
    echo "        ./RunBindEdxInRapidityMtM0.bash [OPTION] ... [CONFIG FILE]"
    echo "    REQUIRED ARGUMENTS:"
    echo "        configuration file - name of configuration file with path if applicable"
    echo "    OPTIONS:"
    echo "        -h - display this help information"
    echo "        -n - number of events - by default all events in the tree will be read"
    echo "             this number will superceede the value in the config file"
    echo ""
    echo ""
}

#Check for options
while [ "$#" -gt 0 ]; do
    while getopts "n:h" opts; do
        case "$opts" in
	    n) nUserEvents="${OPTARG}"; shift;;
	    h) help_func; exit 1;;
            ?) exit 1;;
            *) echo "For help use option: -h"; exit 1;;
        esac
        shift
        OPTIND=1
    done
    
    if [ "$#" -gt 0 ]; then
        POSITIONALPARAM=(${POSITIONALPARAM[@]} $1)
        shift
        OPTIND=1
    fi
done

#Make sure only one argument remains after all the options
if [ "${#POSITIONALPARAM[@]}" -ne 1 ]; then
    echo "ERROR: This script requires only one argument. For usage information use option -h."
    echo "       If you have used options check your formating."
    exit 1
fi

#Assign user inputs to variables
configFile=${POSITIONALPARAM[0]}

#Make Sure the configuration variable is set
if [ -z $configFile ]; then
    echo "ERROR: Configuration file was not set. For usage information use option -h"
    exit 1
fi

#Make Sure the configuration file exists
if [ ! -e $configFile ]; then
    echo "ERROR: Configruation file ($configFile) was not found!"
    exit 1
fi

#Get the Configuration from the Config file
source $configFile

#If the user has specified a number of events with the option then use it instead
#of the quantity specified in the config file
if [ ! -z "$nUserEvents" ]; then
    nEvents=$nUserEvents
fi

#Variables used below
processID=()
outFiles=()

for i in ${pidCalibrationInput[@]}; do

    rndStr=$(cat /dev/urandom | tr -dc 'A-Z0-9' | fold -w 12 | head -n 1)
    tempOutFile=$tmpDir/$rndStr.root
    outFiles+=($tempOutFile)
    
    nice root -l -b -q ../macros/RunBindEdxInRapidityMtM0.C\(\"$i\",\"$tempOutFile\",\"$starLib\",$energy,\"$eventConfig\",$nEvents\) > $logDir/BindEdxInRapidityMtm0\_$rndStr.log 2>&1 &

    processID+=($!)

done
wait ${processID[@]}

hadd -f $pidCalibrationFile ${outFiles[@]}

rm ${outFiles[@]}

exit 0
