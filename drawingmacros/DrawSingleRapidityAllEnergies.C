#include "../inc/globalDefinitions.h"
#include "../inc/StyleSettings.h"
#include "CanvasPartition.h"

void DrawSingleRapidityAllEnergies(TString eventConfig, TString system, Int_t iCentBin, Int_t speciesIndex,
				   Double_t rapidity, Bool_t corrected=false){

  Bool_t save = true; //Should the canvas be saved?
  Double_t maxmTm0=1.2;       //Maximum mT-m0 value to draw
  const int nEnergies(7);
  Double_t energies[nEnergies] = {7.7,11.5,14.5,19.6,27.0,39.0,62.4};
  
  gSystem->Load("../bin/utilityFunctions_cxx.so");
  gSystem->Load("../bin/TrackInfo_cxx.so");
  gSystem->Load("../bin/PrimaryVertexInfo_cxx.so");
  gSystem->Load("../bin/EventInfo_cxx.so");
  gSystem->Load("../bin/ParticleInfo_cxx.so");
  gSystem->Load("../bin/StRefMultExtendedCorr_cxx.so");
  gSystem->Load("../bin/UserCuts_cxx.so");
  SetVariableUserCuts(7.7,eventConfig,"");
  ParticleInfo *particleInfo = new ParticleInfo();

  //Spectra Files
  TFile *spectraFile[nEnergies];
  spectraFile[0] = new TFile(Form("../userfiles/AuAu07/outputFiles/AuAu07_%s_Spectra.root",eventConfig.Data()),"READ");
  spectraFile[1] = new TFile(Form("../userfiles/AuAu11/outputFiles/AuAu11_%s_Spectra.root",eventConfig.Data()),"READ");
  spectraFile[2] = new TFile(Form("../userfiles/AuAu14/outputFiles/AuAu14_%s_Spectra.root",eventConfig.Data()),"READ");
  spectraFile[3] = new TFile(Form("../userfiles/AuAu19/outputFiles/AuAu19_%s_Spectra.root",eventConfig.Data()),"READ");
  spectraFile[4] = new TFile(Form("../userfiles/AuAu27/outputFiles/AuAu27_%s_Spectra.root",eventConfig.Data()),"READ");
  spectraFile[5] = new TFile(Form("../userfiles/AuAu39/outputFiles/AuAu39_%s_Spectra.root",eventConfig.Data()),"READ");
  spectraFile[6] = new TFile(Form("../userfiles/AuAu62/outputFiles/AuAu62_%s_Spectra.root",eventConfig.Data()),"READ");

  
  //Create the Spectra Name
  TString type = "raw";
  TString Type = "Raw";
  TString name = "Uncorrected";
  if (corrected){
    type = "corrected";
    Type = "Corrected";
    name = "Corrected";
  }

  Double_t yScaleMinPlus(0), yScaleMaxPlus(0);
  Double_t yScaleMinMinus(0), yScaleMaxMinus(0);
  if (speciesIndex == PION){
    yScaleMinPlus = .25;
    yScaleMaxPlus = 700;
    yScaleMinMinus = yScaleMinPlus;
    yScaleMaxMinus = yScaleMaxPlus;
  }
  else if (speciesIndex == PROTON){
    yScaleMinPlus = .15;
    yScaleMaxPlus = 50;
    yScaleMinMinus = .0015;
    yScaleMaxMinus = 12;
  }
  else if (speciesIndex == KAON){
    yScaleMinPlus = .025;
    yScaleMaxPlus = 30;
    yScaleMinMinus = yScaleMinPlus;
    yScaleMaxMinus = yScaleMaxPlus;
  }
  
  Int_t yIndex = GetRapidityIndex(rapidity);
  const int nCentralityBins = GetNCentralityBins();

  //Create the Canvas
  Int_t nPadsX(1);
  Int_t nPadsY(2);
  TCanvas *canvas = new TCanvas(Form("%sAllEnergies_%s_%s_yIndex%d",system.Data(),
				     particleInfo->GetParticleName(speciesIndex).Data(),eventConfig.Data(),yIndex),
				"",5,20,600*nPadsX,500*nPadsY);
  
  //Partition the Canvas
  CanvasPartition(canvas,nPadsX,nPadsY,.15,.02,.05,.02);

  //Loop Over the Pads and Draw
  for (int iPadX=0; iPadX<nPadsX; iPadX++){
    for (int iPadY=0; iPadY<nPadsY; iPadY++){

      TString speciesName = particleInfo->GetParticleName(speciesIndex);
      int charge(0);
      if (iPadX == 0 && iPadY==0)
	charge = 1;
      else
	charge = -1;
      
      gPad = (TPad *)gROOT->FindObject(Form("pad_%i_%i",iPadX,iPadY));
      gPad->SetLogy();
      gPad->SetTicks(1,1);
      TH1F *frame = gPad->DrawFrame(-0.05,charge<0 ? yScaleMinMinus:yScaleMinPlus,
				    maxmTm0+.2,charge<0 ? yScaleMaxMinus:yScaleMaxPlus);
      frame->GetXaxis()->SetTitle(Form("m_{T}-m_{%s} (GeV/c^{2})",
				       particleInfo->GetParticleSymbol(speciesIndex).Data()));

      //X Axis Title
      frame->GetXaxis()->SetLabelFont(63);
      frame->GetXaxis()->SetLabelSize(18);
      frame->GetXaxis()->SetTitleFont(63);
      frame->GetXaxis()->SetTitleSize(20);
      frame->GetXaxis()->SetNdivisions(510);
      frame->GetXaxis()->SetTitleOffset(2.0);

      if (iPadX==0){
	
	//Axis Titles
	frame->GetYaxis()->SetTitle("#frac{1}{N_{Evt}}#times#frac{1}{2#pim_{T}}#times#frac{d^{2}N}{dm_{T}dy} (GeV/c^{2})^{2}");
	frame->GetYaxis()->SetTitleOffset(2.9);
	frame->GetYaxis()->SetLabelFont(63);
	frame->GetYaxis()->SetLabelSize(18);
	frame->GetYaxis()->SetTitleFont(63);
	frame->GetYaxis()->SetTitleSize(20);

	TPaveText *title = new TPaveText(.6,iPadY==1 ? .8:.83 ,.95,iPadY==1 ? .92:.95,"NBNDCBR");
	title->SetFillColor(kWhite);
	title->SetBorderSize(0);
	title->SetTextFont(63);
	title->SetTextSize(22);
	title->SetTextAlign(33);
	//title->AddText(Form("%s Spectra %s",
	//		    particleInfo->GetParticleSymbol(speciesIndex,charge).Data(),
	//		    system.Data()));
	title->AddText(Form("%s %d-%d%% Au+Au",
			    particleInfo->GetParticleSymbol(speciesIndex,charge).Data(),
			    iCentBin!=nCentralityBins-1 ? (int)GetCentralityPercents().at(iCentBin+1):0,
			    (int)GetCentralityPercents().at(iCentBin)));
	title->AddText(Form("y_{%s}=[%.03g,%.03g]",particleInfo->GetParticleSymbol(speciesIndex).Data(),
			    GetRapidityRangeLow(yIndex),GetRapidityRangeHigh(yIndex)));
	title->Draw("SAME");

	TLegend *leg = new TLegend(.2,iPadY==1 ? .04:.13,.45,iPadY==1 ? .31:.4);
	leg->SetLineColor(kWhite);
	leg->SetBorderSize(0);
	leg->SetFillColor(kWhite);
	leg->SetTextFont(63);
	leg->SetTextSize(18);
	leg->SetNColumns(2);
	leg->SetHeader("#sqrt{s_{NN}} GeV");
	//Gets Filled in loop below

      }//End if iPadX==0

      //Loop Over the Energy Files and Draw the Spectra
      for (int iFile=0; iFile<nEnergies; iFile++){


	
	TGraphErrors *spectra=NULL, *spectraTOF=NULL;
	
	TString spectraName(Form("%sSpectra_%s_Cent%02d_yIndex%02d",
				 type.Data(),particleInfo->GetParticleName(speciesIndex,charge).Data(),
				 iCentBin,yIndex));
	TString spectraNameTOF(Form("%sSpectraTOF_%s_Cent%02d_yIndex%02d",
				    type.Data(),particleInfo->GetParticleName(speciesIndex,charge).Data(),
				    iCentBin,yIndex));
	
	spectra = (TGraphErrors *)spectraFile[iFile]->
	  Get(Form("%sSpectra_%s/%s",Type.Data(),
		   particleInfo->GetParticleName(speciesIndex,charge).Data(),
		   spectraName.Data()));
	spectraTOF = (TGraphErrors *)spectraFile[iFile]->
	  Get(Form("%sSpectra_%s/%s",Type.Data(),
		   particleInfo->GetParticleName(speciesIndex,charge).Data(),
		   spectraNameTOF.Data()));

	if (!spectra)
	  continue;
	
	//Remove high mTm0 Point
	TGraphChop(spectra,maxmTm0,false);
	if (spectraTOF){
	  if (spectraTOF->GetN())
	    TGraphChop(spectraTOF,maxmTm0,false);
	  else
	    spectraTOF = NULL;
	}
	
	spectra->SetMarkerColor(GetEnergyColor(energies[iFile]));
	if (spectraTOF)
	  spectraTOF->SetMarkerColor(GetEnergyColor(energies[iFile]));
	
	spectra->Draw("PZ");
	if (spectraTOF)
	  spectraTOF->Draw("PZ");

	if (iPadX == 0){
	  leg->AddEntry(spectra,Form("%.01f",energies[iFile]),"P");
	}

	
      }

      leg->Draw("SAME");
      gPad->Modified();
      gPad->Update();
    }//End Loop Over yPads
  }//End Loop Over xpads

  if (save){
    canvas->Print(Form("%s.gif",canvas->GetName()));
  }
  
}
