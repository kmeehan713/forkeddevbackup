//Draws the Rapidity Density Distributions of the Top 5% Pion Plus and Minus for each energy
//Fits the distributions with a gaussian
//Draws the width of the gaussian as a function of collision energy

#include "../../inc/globalDefinitions.h"
#include "../../inc/StyleSettings.h"
#include "../CanvasPartition.h"

const int nEnergies(7);

//____________________________________________________________________________________
void DrawRapidityWidth(TString eventConfig, TString system){

  gStyle->SetOptStat(0);
  
  bool save = false;
  const int pid = 0; //PION

  //Load the Necessary Libraries
  gSystem->Load("../../bin/utilityFunctions_cxx.so");
  gSystem->Load("../../bin/TrackInfo_cxx.so");
  gSystem->Load("../../bin/PrimaryVertexInfo_cxx.so");
  gSystem->Load("../../bin/EventInfo_cxx.so");
  gSystem->Load("../../bin/ParticleInfo_cxx.so");
  gSystem->Load("../../bin/StRefMultExtendedCorr_cxx.so");
  gSystem->Load("../../bin/UserCuts_cxx.so");
  gSystem->Load("../../bin/SpectraClass_cxx.so");
  SetVariableUserCuts(7.7,eventConfig,"");
  ParticleInfo particleInfo;

  //************************************************
  //Create the Canvases
  TCanvas *rapidityDensityCanvas = new TCanvas("RapidityDensityCanvas","RapidityDensityCanvas",20,20,1600,800);
  CanvasPartition(rapidityDensityCanvas,2,1,.075);
  rapidityDensityCanvas->cd();
  //return;
  //Draw the Frames
  gPad = (TPad *)gROOT->FindObject("pad_2");
  gPad->SetTicks();
  TH1F *pionPlusFrame = gPad->DrawFrame(-2.2,0,2.2,260);

  pionPlusFrame->SetTitle(Form(";y_{%s};dN/dy",particleInfo.GetParticleSymbol(pid,1).Data()));

  pionPlusFrame->GetYaxis()->SetTitleFont(63);
  pionPlusFrame->GetYaxis()->SetTitleSize(35);
  pionPlusFrame->GetYaxis()->SetTitleOffset(1.4);
  pionPlusFrame->GetYaxis()->SetLabelFont(63);
  pionPlusFrame->GetYaxis()->SetLabelSize(30);

  pionPlusFrame->GetXaxis()->SetTitleFont(63);
  pionPlusFrame->GetXaxis()->SetTitleSize(35);
  pionPlusFrame->GetXaxis()->SetLabelFont(63);
  pionPlusFrame->GetXaxis()->SetLabelSize(30);

  TPaveText *title = new TPaveText(.18,.847,.22,.95,"NDC");
  title->SetFillColor(kWhite);
  title->SetBorderSize(0);
  title->SetTextFont(63);
  title->SetTextSize(40);
  title->SetTextAlign(11);
  title->AddText(Form("Rapidity Density"));
  title->AddText(Form("#scale[.85]{%s Top 5%% Centrality}",system.Data()));
  title->Draw();

  TPaveText *pidTitlePlus = new TPaveText(1.55,225,1.85,245);
  pidTitlePlus->SetFillColor(kWhite);
  pidTitlePlus->SetBorderSize(0);
  pidTitlePlus->SetTextFont(63);
  pidTitlePlus->SetTextSize(55);
  pidTitlePlus->SetTextAlign(22);
  pidTitlePlus->AddText(Form("%s",particleInfo.GetParticleSymbol(pid,1).Data()));
  pidTitlePlus->Draw();
  
  gPad = (TPad *)gROOT->FindObject("pad_3");
  gPad->SetTicks();
  TH1F *pionMinusFrame = gPad->DrawFrame(-2.2,0,2.2,260);

  pionMinusFrame->SetTitle(Form(";y_{%s};dN/dy",particleInfo.GetParticleSymbol(pid,-1).Data()));
  
  pionMinusFrame->GetXaxis()->SetTitleFont(63);
  pionMinusFrame->GetXaxis()->SetTitleSize(35);
  pionMinusFrame->GetXaxis()->SetLabelFont(63);
  pionMinusFrame->GetXaxis()->SetLabelSize(30);

  TPaveText *pidTitleMinus = new TPaveText(1.55,225,1.85,245);
  pidTitleMinus->SetFillColor(kWhite);
  pidTitleMinus->SetBorderSize(0);
  pidTitleMinus->SetTextFont(63);
  pidTitleMinus->SetTextSize(55);
  pidTitleMinus->SetTextAlign(22);
  pidTitleMinus->AddText(Form("%s",particleInfo.GetParticleSymbol(pid,-1).Data()));
  pidTitleMinus->Draw();


  
  //************************************************
  //Load the Spectra Files and Draw the dNdy Distributions
  TFile *file[nEnergies];
  for (int iFile=0; iFile<nEnergies; iFile++){
    file[iFile] = new TFile(Form("../../userfiles/%s%02d/outputFiles/%s%02d_%s_Spectra.root",
				 system.Data(),(int)GetEnergy(iFile),
				 system.Data(),(int)GetEnergy(iFile),eventConfig.Data()),"READ");
  }

  //Get the dNdY Distributions and Draw Them
  TH1F *dNdyPlusHistos[nEnergies][2];//[energies][cent Bins][meas/reflected]
  TH1F *dNdyMinusHistos[nEnergies][2];
  TGraphAsymmErrors *dNdyPlusGraphSys[nEnergies][2];
  TGraphAsymmErrors *dNdyMinusGraphSys[nEnergies][2];
  TF1 *fitFuncsPlus[nEnergies][2];
  TF1 *fitFuncsMinus[nEnergies][2];
  double fourPiYieldPlusVals[nEnergies];
  double fourPiYieldMinusVals[nEnergies];
  double fourPiYieldPlusErrs[nEnergies];
  double fourPiYieldMinusErrs[nEnergies];
  
  const int iCentBin(8);
  for (int iFile=0; iFile<nEnergies; iFile++){
    for (int i=0; i<2; i++){
      
      dNdyPlusHistos[iFile][i] =
	(TH1F *)file[iFile]->Get(Form("dNdyDistributions_%s/dNdyHisto_%s_Cent%02d%s",
				      particleInfo.GetParticleName(pid,1).Data(),
				      particleInfo.GetParticleName(pid,1).Data(),
				      iCentBin,
				      i == 0 ? "" : "_Reflected"));
      dNdyPlusHistos[iFile][i]->SetMarkerColor(GetEnergyColor(GetEnergy(iFile)));
      dNdyPlusHistos[iFile][i]->SetMarkerSize(1.7);
      
      dNdyMinusHistos[iFile][i] =
	(TH1F *)file[iFile]->Get(Form("dNdyDistributions_%s/dNdyHisto_%s_Cent%02d%s",
				      particleInfo.GetParticleName(pid,-1).Data(),
				      particleInfo.GetParticleName(pid,-1).Data(),
				      iCentBin,
				      i == 0 ? "" : "_Reflected"));
      dNdyMinusHistos[iFile][i]->SetMarkerColor(GetEnergyColor(GetEnergy(iFile)));
      dNdyMinusHistos[iFile][i]->SetMarkerSize(1.7);

      dNdyPlusGraphSys[iFile][i] =
	(TGraphAsymmErrors *)file[iFile]->Get(Form("dNdyDistributions_%s/dNdyHisto_%s_Cent%02d%s_Sys",
						   particleInfo.GetParticleName(pid,1).Data(),
						   particleInfo.GetParticleName(pid,1).Data(),
						   iCentBin,
						   i == 0 ? "" : "_Reflected"));
      dNdyPlusGraphSys[iFile][i]->SetFillColor(GetEnergyColor(GetEnergy(iFile)));

      dNdyMinusGraphSys[iFile][i] =
	(TGraphAsymmErrors *)file[iFile]->Get(Form("dNdyDistributions_%s/dNdyHisto_%s_Cent%02d%s_Sys",
						   particleInfo.GetParticleName(pid,-1).Data(),
						   particleInfo.GetParticleName(pid,-1).Data(),
						   iCentBin,
						   i == 0 ? "" : "_Reflected"));
      dNdyMinusGraphSys[iFile][i]->SetFillColor(GetEnergyColor(GetEnergy(iFile)));

      fitFuncsPlus[iFile][i] = new TF1(Form("FitFunc_%s_energy%02d_Cent%02d%s",
					    particleInfo.GetParticleName(pid,1).Data(),
					    iFile,iCentBin,
					    i == 0 ? "" : "_Sys"),
				       "gaus(0)",
				       i == 0 ? -.05 : -10,
				       i == 0 ? 10 : -.05);
      fitFuncsMinus[iFile][i] = new TF1(Form("FitFunc_%s_energy%02d_Cent%02d%s",
					     particleInfo.GetParticleName(pid,1).Data(),
					     iFile,iCentBin,
					     i == 0 ? "" : "_Sys"),
				       "gaus(0)",
					i == 0 ? -.05 : -10,
					i == 0 ? 10 : -.05);

      fitFuncsPlus[iFile][i]->SetParameters(100,0.0,1.5);
      fitFuncsPlus[iFile][i]->FixParameter(1,0.0);
      fitFuncsPlus[iFile][i]->SetParLimits(0,60,220);
      fitFuncsPlus[iFile][i]->SetParLimits(2,.8,2.5);

      fitFuncsMinus[iFile][i]->SetParameters(100,0.0,1.5);
      fitFuncsMinus[iFile][i]->FixParameter(1,0.0);
      fitFuncsMinus[iFile][i]->SetParLimits(0,60,220);
      fitFuncsMinus[iFile][i]->SetParLimits(2,.8,2.5);

      //If i=0 then this is the measured spectrum so fit it
      //If i=1 then this is the reflected spectrum so transfer the parameters from the fit
      if (i==0){
	dNdyPlusHistos[iFile][i]->Fit(fitFuncsPlus[iFile][i],"IN","",-.05,10);

	//fourPiYieldPlusVals[iFile] = fitFuncsPlus[iFile][i]->GetParameter(0)*fitFuncsPlus[iFile][i]->GetParameter(2) * sqrt(TMath::TwoPi());
	fourPiYieldPlusVals[iFile] = fitFuncsPlus[iFile][i]->Integral(-50,50);
	fourPiYieldPlusErrs[iFile] = fitFuncsPlus[iFile][i]->IntegralError(-50,50);
	
	dNdyMinusHistos[iFile][i]->Fit(fitFuncsMinus[iFile][i],"IN","",-.05,10);

	fourPiYieldMinusVals[iFile] = fitFuncsMinus[iFile][i]->Integral(-50,50);
	fourPiYieldMinusErrs[iFile] = fitFuncsMinus[iFile][i]->IntegralError(-50,50);

	fitFuncsPlus[iFile][i]->SetLineColor(GetEnergyColor(GetEnergy(iFile)));
	fitFuncsMinus[iFile][i]->SetLineColor(GetEnergyColor(GetEnergy(iFile)));

	fitFuncsPlus[iFile][i]->SetLineStyle(1);
	fitFuncsMinus[iFile][i]->SetLineStyle(1);
      }
      else {
	fitFuncsPlus[iFile][i]->SetParameters(fitFuncsPlus[iFile][0]->GetParameters());
	fitFuncsMinus[iFile][i]->SetParameters(fitFuncsMinus[iFile][0]->GetParameters());

	fitFuncsPlus[iFile][i]->SetLineColor(GetEnergyColor(GetEnergy(iFile)));
	fitFuncsMinus[iFile][i]->SetLineColor(GetEnergyColor(GetEnergy(iFile)));

	fitFuncsPlus[iFile][i]->SetLineStyle(7);
	fitFuncsMinus[iFile][i]->SetLineStyle(7);
      }
      
      rapidityDensityCanvas->cd();
      gPad = (TPad *)gROOT->FindObject("pad_2");
      //dNdyPlusGraphSys[iFile][i]->Draw("Z");
      dNdyPlusHistos[iFile][i]->Draw("SAME E");
      fitFuncsPlus[iFile][i]->Draw("SAME");
      gPad->Update();
      
      TPaveStats *statsBox = (TPaveStats *)gPad->GetPrimitive("stats");
      if (statsBox)
	delete statsBox;
      
      gPad->IsModified();
      gPad->Update();
      
      gPad = (TPad *)gROOT->FindObject("pad_3");
      //dNdyMinusGraphSys[iFile][i]->Draw("2");
      dNdyMinusHistos[iFile][i]->Draw("SAME E");
      fitFuncsMinus[iFile][i]->Draw("SAME");
      gPad->Update();
      
      statsBox = (TPaveStats *)gPad->GetPrimitive("stats");
      if (statsBox)
	delete statsBox;
      
      gPad->IsModified();
      gPad->Update();
      
    }//End Loop Over Charges
  }//End Loop Over Energies

  //Create the Energy Legend
  rapidityDensityCanvas->cd();
  gPad = (TPad *)gROOT->FindObject("pad_2");
  TLegend *energyLeg = new TLegend(.4,.13,.73,.26);
  energyLeg->SetFillColor(kWhite);
  energyLeg->SetBorderSize(0);
  energyLeg->SetNColumns(4);
  energyLeg->SetTextFont(63);
  energyLeg->SetTextSize(25);
  energyLeg->SetTextAlign(22);
  energyLeg->SetHeader("#sqrt{s_{NN}} (GeV)");
  for (int iEnergy=0; iEnergy<nEnergies; iEnergy++){
    energyLeg->AddEntry(dNdyPlusHistos[iEnergy][0],Form("%.1f",GetEnergy(iEnergy)),"P");
  }
  energyLeg->Draw("SAME");

  //Miscellaneous Items
  TMarker *measMarker = new TMarker(0,0,kFullCircle);
  TMarker *refMarker = new TMarker(0,0,kOpenCircle);
  measMarker->SetMarkerColor(kGray+3);
  refMarker->SetMarkerColor(kGray+3);
  measMarker->SetMarkerSize(1.7);
  refMarker->SetMarkerSize(1.7);

  TF1 *measFit = new TF1("measFit","gaus(0)",-1,1);
  TF1 *refFit = new TF1("refFit","gaus(0)",-1,1);
  measFit->SetLineColor(kGray+3);
  refFit->SetLineColor(kGray+3);
  measFit->SetLineStyle(1);
  refFit->SetLineStyle(7);
    
  
  gPad = (TPad *)gROOT->FindObject("pad_3");
  TLegend *miscLeg = new TLegend(.25,.13,.79,.21);
  miscLeg->SetFillColor(kWhite);
  miscLeg->SetBorderSize(0);
  miscLeg->SetNColumns(2);
  miscLeg->SetTextFont(63);
  miscLeg->SetTextSize(25);
  miscLeg->SetTextAlign(12);
  miscLeg->AddEntry(refMarker,"Reflected","P");
  miscLeg->AddEntry(measMarker,"Measured","P");
  miscLeg->AddEntry(refFit,"Reflected","L");
  miscLeg->AddEntry(measFit,"Gaussian Fit","L");

  miscLeg->Draw("SAME");
  

  //************************************************************
  //Rapidity Width Plots
  //Sigma Landau Function
  TF1 *sigmaLandau = new TF1("sigmaLandau","TMath::Sqrt(TMath::Log(x/(2*[0])))",2,250);
  sigmaLandau->FixParameter(0,particleInfo.GetParticleMass(PROTON));
  sigmaLandau->SetLineStyle(1);
  sigmaLandau->SetLineColor(kGray+2);
  sigmaLandau->SetLineWidth(3);
  sigmaLandau->SetNpx(100000);
  
  //World Data
  TGraphErrors *widthAGS = new TGraphErrors("../WorldData/DaleValues/dale_AGS.txt","%lg %lg %lg %lg","\t"); 
  TGraphErrors *widthSPS = new TGraphErrors("../WorldData/DaleValues/dale_SPS.txt","%lg %lg %lg %lg","\t"); 
  TGraphErrors *widthRHIC = new TGraphErrors("../WorldData/DaleValues/dale_RHIC.txt","%lg %lg %lg %lg","\t");

  TGraphErrors *fourPiAGS = new TGraphErrors("../WorldData/FourPiYields/fourPiYield_AGS.txt",
					     "%lg %lg %lg %lg","\t");
  TGraphErrors *fourPiSPS = new TGraphErrors("../WorldData/FourPiYields/fourPiYield_SPS.txt",
					     "%lg %lg %lg %lg","\t");
  TGraphErrors *fourPiRHIC = new TGraphErrors("../WorldData/FourPiYields/fourPiYield_RHIC.txt",
					      "%lg %lg %lg %lg","\t");

  TGraphErrors *nPartAGS = new TGraphErrors("../WorldData/NpartValues/nPart_AGS.txt",
					    "%lg %lg %lg %lg","\t");
  TGraphErrors *nPartSPS = new TGraphErrors("../WorldData/NpartValues/nPart_SPS.txt",
					    "%lg %lg %lg %lg","\t");
  TGraphErrors *nPartRHIC = new TGraphErrors("../WorldData/NpartValues/nPart_RHIC.txt",
					     "%lg %lg %lg %lg","\t");

  widthAGS->SetMarkerStyle(kFullTriangleUp);
  widthAGS->SetMarkerColor(kGray+1);
  widthAGS->SetMarkerSize(2.0);

  widthSPS->SetMarkerStyle(kFullSquare);
  widthSPS->SetMarkerColor(kGray+1);
  widthSPS->SetMarkerSize(1.8);

  widthRHIC->SetMarkerStyle(kFullDiamond);
  widthRHIC->SetMarkerColor(kGray+1);
  widthRHIC->SetMarkerSize(2.2);

  fourPiAGS->SetMarkerStyle(kFullTriangleUp);
  fourPiAGS->SetMarkerColor(kGray+1);
  fourPiAGS->SetMarkerSize(2.0);
  
  fourPiSPS->SetMarkerStyle(kFullSquare);
  fourPiSPS->SetMarkerColor(kGray+1);
  fourPiSPS->SetMarkerSize(1.8);
  
  fourPiRHIC->SetMarkerStyle(kFullDiamond);
  fourPiRHIC->SetMarkerColor(kGray+1);
  fourPiRHIC->SetMarkerSize(2.2);

  TGraphErrors *daleAGS = new TGraphErrors(*widthAGS);
  TGraphErrors *daleSPS = new TGraphErrors(*widthSPS);
  TGraphErrors *daleRHIC = new TGraphErrors(*widthRHIC);

  for (int iPoint=0; iPoint<daleAGS->GetN(); iPoint++){
    //Dale Graph
    daleAGS->SetPoint(iPoint,daleAGS->GetX()[iPoint],
		       daleAGS->GetY()[iPoint]/sigmaLandau->Eval(daleAGS->GetX()[iPoint]));
    daleAGS->SetPointError(iPoint,0,
			    daleAGS->GetY()[iPoint]*(daleAGS->GetEY()[iPoint]/daleAGS->GetY()[iPoint]));

    //Four Pi Yield Graph
    double ratio = fourPiAGS->GetY()[iPoint]/nPartAGS->GetY()[iPoint];
    fourPiAGS->SetPointError(iPoint,0,ratio * sqrt(pow(fourPiAGS->GetEY()[iPoint]/fourPiAGS->GetY()[iPoint],2) +
						   pow(nPartAGS->GetEY()[iPoint]/nPartAGS->GetY()[iPoint],2)));
    fourPiAGS->SetPoint(iPoint,fourPiAGS->GetX()[iPoint],ratio);
  }

  for (int iPoint=0; iPoint<daleSPS->GetN(); iPoint++){
    //Dale Graph
    daleSPS->SetPoint(iPoint,daleSPS->GetX()[iPoint],
		       daleSPS->GetY()[iPoint]/sigmaLandau->Eval(daleSPS->GetX()[iPoint]));
    daleSPS->SetPointError(iPoint,0,
			    daleSPS->GetY()[iPoint]*(daleSPS->GetEY()[iPoint]/daleSPS->GetY()[iPoint]));

    //Four Pi Yield Graph
    double ratio = fourPiSPS->GetY()[iPoint]/nPartSPS->GetY()[iPoint];
    fourPiSPS->SetPointError(iPoint,0,ratio * sqrt(pow(fourPiSPS->GetEY()[iPoint]/fourPiSPS->GetY()[iPoint],2) +
						   pow(nPartSPS->GetEY()[iPoint]/nPartSPS->GetY()[iPoint],2)));
    fourPiSPS->SetPoint(iPoint,fourPiSPS->GetX()[iPoint],ratio);
 
  }

  for (int iPoint=0; iPoint<daleRHIC->GetN(); iPoint++){
    //Dale Graph
    daleRHIC->SetPoint(iPoint,daleRHIC->GetX()[iPoint],
		       daleRHIC->GetY()[iPoint]/sigmaLandau->Eval(daleRHIC->GetX()[iPoint]));
    daleRHIC->SetPointError(iPoint,0,
			    daleRHIC->GetY()[iPoint]*(daleRHIC->GetEY()[iPoint]/daleRHIC->GetY()[iPoint]));

    //Four Pi Yield Graph
    double ratio = fourPiRHIC->GetY()[iPoint]/nPartRHIC->GetY()[iPoint];
    fourPiRHIC->SetPointError(iPoint,0,ratio * sqrt(pow(fourPiRHIC->GetEY()[iPoint]/fourPiRHIC->GetY()[iPoint],2) +
						    pow(nPartRHIC->GetEY()[iPoint]/nPartRHIC->GetY()[iPoint],2)));
    fourPiRHIC->SetPoint(iPoint,fourPiRHIC->GetX()[iPoint],ratio);
    
  }


  //////////
  //Rapidity Width
  TCanvas *widthCanvas = new TCanvas("rapidityWidthCanvas","rapidityWidthCanvas",20,20,800,600);
  widthCanvas->SetTopMargin(.025);
  widthCanvas->SetRightMargin(.025);
  widthCanvas->SetBottomMargin(.15);
  widthCanvas->SetLeftMargin(.1);
  widthCanvas->SetTicks();
  widthCanvas->SetLogx();
  
  TH1F *widthFrame = widthCanvas->DrawFrame(.95,0,300,3.1);
  widthFrame->SetTitle(";#sqrt{s_{NN}} (GeV);#sigma(#pi)");
  widthFrame->GetXaxis()->SetTitleFont(63);
  widthFrame->GetXaxis()->SetTitleSize(30);
  widthFrame->GetXaxis()->SetLabelFont(63);
  widthFrame->GetXaxis()->SetLabelSize(25);
  widthFrame->GetXaxis()->SetTitleOffset(1.2);

  widthFrame->GetYaxis()->SetTitleFont(63);
  widthFrame->GetYaxis()->SetTitleSize(30);
  widthFrame->GetYaxis()->SetLabelFont(63);
  widthFrame->GetYaxis()->SetLabelSize(25);
  widthFrame->GetYaxis()->SetTitleOffset(.85);

  TGraphErrors *besWidthGraphMinus = new TGraphErrors();
  besWidthGraphMinus->SetName("besWidthGraphMinus");
  besWidthGraphMinus->SetMarkerStyle(kFullStar);
  besWidthGraphMinus->SetMarkerColor(kRed);
  besWidthGraphMinus->SetMarkerSize(2.5);

  TGraphErrors *besWidthGraphPlus = new TGraphErrors(*besWidthGraphMinus);
  besWidthGraphPlus->SetMarkerStyle(kOpenStar);

  for (int iEnergy=0; iEnergy<nEnergies; iEnergy++){

    besWidthGraphMinus->SetPoint(besWidthGraphMinus->GetN(),GetEnergy(iEnergy),
				 fitFuncsMinus[iEnergy][0]->GetParameter(2));
    besWidthGraphMinus->SetPointError(besWidthGraphMinus->GetN()-1,0,
				      fitFuncsMinus[iEnergy][0]->GetParError(2));
    
    besWidthGraphPlus->SetPoint(besWidthGraphPlus->GetN(),GetEnergy(iEnergy)*1.1,
				 fitFuncsPlus[iEnergy][0]->GetParameter(2));
    besWidthGraphPlus->SetPointError(besWidthGraphPlus->GetN()-1,0,
				      fitFuncsPlus[iEnergy][0]->GetParError(2));
    
    
  }

  sigmaLandau->Draw("SAME");
  widthAGS->Draw("PZ");
  widthSPS->Draw("PZ");
  widthRHIC->Draw("PZ");
  besWidthGraphMinus->Draw("PZ");
  besWidthGraphPlus->Draw("PZ");

  TPaveText *rapidityWidthTitle = new TPaveText(.11,.83,.55,.945,"NDC");
  rapidityWidthTitle->SetBorderSize(0);
  rapidityWidthTitle->SetFillColor(kWhite);
  rapidityWidthTitle->SetTextFont(63);
  rapidityWidthTitle->SetTextSize(35);
  rapidityWidthTitle->SetTextAlign(12);
  rapidityWidthTitle->AddText("Rapidity Width of #pi^{#pm}");
  rapidityWidthTitle->AddText("#scale[.8]{Most Central}");
  rapidityWidthTitle->Draw();

  TLegend *widthLegend = new TLegend(.12,.68,.53,.82);
  widthLegend->SetBorderSize(0);
  widthLegend->SetFillColor(kWhite);
  widthLegend->SetTextFont(63);
  widthLegend->SetTextSize(25);
  widthLegend->SetNColumns(2);
  widthLegend->AddEntry(widthAGS,"E895 #pi^{-}","P");
  widthLegend->AddEntry(besWidthGraphMinus,"STAR #pi^{-}","P");
  widthLegend->AddEntry(widthSPS,"NA49 #pi^{-}","P"); 
  widthLegend->AddEntry(besWidthGraphPlus,"STAR #pi^{+} *","P");
  widthLegend->AddEntry(widthRHIC,"BRAHMS #pi^{-}","P");
  widthLegend->Draw("SAME");

  TLegend *sigmaLandauLeg = new TLegend(.61,.2,.92,.31);
  sigmaLandauLeg->SetBorderSize(0);
  sigmaLandauLeg->SetFillColor(kWhite);
  sigmaLandauLeg->SetTextFont(63);
  sigmaLandauLeg->SetTextSize(25);
  sigmaLandauLeg->AddEntry(sigmaLandau,"#sigma_{Landau} = #sqrt{#scale[.8]{ln#left(#frac{#sqrt{s_{NN}}}{2m_{p}}#right)}}","L");
  sigmaLandauLeg->Draw("SAME");
  widthCanvas->Update();

  TPaveText *widthFooter = new TPaveText(.08,.02,.44,.06,"NDC");
  widthFooter->SetFillColor(kWhite);
  widthFooter->SetBorderSize(0);
  widthFooter->SetTextFont(63);
  widthFooter->SetTextSize(15);
  widthFooter->SetTextAlign(12);
  widthFooter->AddText("* Shifted to higher#sqrt{s_{NN}} by 10% for clarity.");
  widthFooter->Draw();

  /////////////
  //Dale
  TCanvas *daleCanvas = new TCanvas ("DaleCanvas","DaleCanvas",900,20,800,600);
  daleCanvas->SetTopMargin(.025);
  daleCanvas->SetRightMargin(.025);
  daleCanvas->SetBottomMargin(.15);
  daleCanvas->SetLeftMargin(.13);
  daleCanvas->SetTicks();
  daleCanvas->SetLogx();
  
  TH1F *daleFrame = daleCanvas->DrawFrame(.95,.65,300,1.49);
  daleFrame->SetTitle(";#sqrt{s_{NN}} (GeV);#sigma(#pi)/#sigma_{Landau}");
  daleFrame->GetXaxis()->SetTitleFont(63);
  daleFrame->GetXaxis()->SetTitleSize(30);
  daleFrame->GetXaxis()->SetLabelFont(63);
  daleFrame->GetXaxis()->SetLabelSize(25);
  daleFrame->GetXaxis()->SetTitleOffset(1.2);

  daleFrame->GetYaxis()->SetTitleFont(63);
  daleFrame->GetYaxis()->SetTitleSize(30);
  daleFrame->GetYaxis()->SetLabelFont(63);
  daleFrame->GetYaxis()->SetLabelSize(25);
  daleFrame->GetYaxis()->SetTitleOffset(1.16);

  TLine *unityLine = new TLine(.95,1,300,1);
  unityLine->SetLineWidth(4);
  unityLine->SetLineColor(kGray+2);
  unityLine->SetLineStyle(7);
  unityLine->Draw("SAME");



  daleAGS->Draw("PZ");
  daleSPS->Draw("PZ");
  daleRHIC->Draw("PZ");
  
  TGraphErrors *besDaleGraphMinus = new TGraphErrors();
  besDaleGraphMinus->SetName("besDaleGraphMinus");
  besDaleGraphMinus->SetMarkerStyle(kFullStar);
  besDaleGraphMinus->SetMarkerColor(kRed);
  besDaleGraphMinus->SetMarkerSize(2.5);

  TGraphErrors *besDaleGraphPlus = new TGraphErrors(*besDaleGraphMinus);
  besDaleGraphPlus->SetMarkerStyle(kOpenStar);

  for (int iPoint=0; iPoint<besWidthGraphMinus->GetN(); iPoint++){

    double energy = besWidthGraphMinus->GetX()[iPoint];
    
    besDaleGraphMinus->SetPoint(iPoint,energy,
				besWidthGraphMinus->GetY()[iPoint] / sigmaLandau->Eval(energy));
    besDaleGraphMinus->SetPointError(iPoint,0,
				     besDaleGraphMinus->GetY()[iPoint] *
				     (besWidthGraphMinus->GetEY()[iPoint] / besWidthGraphMinus->GetY()[iPoint]));

    besDaleGraphPlus->SetPoint(iPoint,energy*1.1,
			       besWidthGraphPlus->GetY()[iPoint] / sigmaLandau->Eval(energy));
    besDaleGraphPlus->SetPointError(iPoint,0,
				    besDaleGraphPlus->GetY()[iPoint] *
				    (besWidthGraphPlus->GetEY()[iPoint] / besWidthGraphPlus->GetY()[iPoint]));

  }

  TGraphErrors *weightedAverageGraph = new TGraphErrors();
  weightedAverageGraph->SetName("WeightedAverageDaleGraph");
  weightedAverageGraph->SetFillColor(kRed-7);
  weightedAverageGraph->SetFillStyle(3003);
  weightedAverageGraph->SetLineColor(kRed);
  weightedAverageGraph->SetLineStyle(7);
  weightedAverageGraph->SetLineWidth(3);
  for (int iPoint=0; iPoint<besDaleGraphMinus->GetN(); iPoint++){

    double energy = besDaleGraphMinus->GetX()[iPoint];
    if (iPoint == besDaleGraphMinus->GetN()-1)
      energy = energy*1.1;

    double vals[2] = {besDaleGraphMinus->GetY()[iPoint],
		      besDaleGraphPlus->GetY()[iPoint]};
    double weights[2] = {pow(besDaleGraphMinus->GetEY()[iPoint],-2),
			 pow(besDaleGraphPlus->GetEY()[iPoint],-2)};
    double weightedAverageVal = TMath::Mean(2,vals,weights);
    double weightedRMSVal = sqrt(pow(besDaleGraphMinus->GetEY()[iPoint],2) +
				 pow(besDaleGraphPlus->GetEY()[iPoint],2)) / 2.0;

    weightedAverageGraph->SetPoint(iPoint,energy,weightedAverageVal);
    weightedAverageGraph->SetPointError(iPoint,0,weightedRMSVal);
    
  }
  
  //Draw
  weightedAverageGraph->Draw("L3");
  besDaleGraphMinus->Draw("PZ");
  besDaleGraphPlus->Draw("PZ");

  TPaveText *rapidityDaleTitle = new TPaveText(.15,.81,.50,.945,"NDC");
  rapidityDaleTitle->SetBorderSize(0);
  rapidityDaleTitle->SetFillColor(kWhite);
  rapidityDaleTitle->SetTextFont(63);
  rapidityDaleTitle->SetTextSize(35);
  rapidityDaleTitle->SetTextAlign(12);
  rapidityDaleTitle->AddText("Dale Observable");
  rapidityDaleTitle->AddText("#scale[.8]{Most Central}");
  rapidityDaleTitle->Draw();

  TLegend *daleLegend = new TLegend(*widthLegend);
  daleLegend->SetX1NDC(.51);
  daleLegend->SetY1NDC(.18);
  daleLegend->SetX2NDC(.94);
  daleLegend->SetY2NDC(.33);
  daleLegend->AddEntry(weightedAverageGraph,"Wt. Mean","FL");
  daleLegend->Draw("SAME");

  TPaveText *daleFooter = new TPaveText(*widthFooter);
  daleFooter->SetX1NDC(.1);
  daleFooter->Draw();
 
  daleCanvas->Update();

  //////////
  //Four Pi Yield
  TCanvas *fourPiCanvas = new TCanvas("rapidityFourPiCanvas","rapidityFourPiCanvas",20,20,800,600);
  fourPiCanvas->SetTopMargin(.025);
  fourPiCanvas->SetRightMargin(.025);
  fourPiCanvas->SetBottomMargin(.15);
  fourPiCanvas->SetLeftMargin(.1);
  fourPiCanvas->SetTicks();
  fourPiCanvas->SetLogx();
  
  TH1F *fourPiFrame = fourPiCanvas->DrawFrame(.95,0,300,5.5);
  fourPiFrame->SetTitle(";#sqrt{s_{NN}} (GeV);4#pi Yield/#LTN_{Part}#GT");
  fourPiFrame->GetXaxis()->SetTitleFont(63);
  fourPiFrame->GetXaxis()->SetTitleSize(30);
  fourPiFrame->GetXaxis()->SetLabelFont(63);
  fourPiFrame->GetXaxis()->SetLabelSize(25);
  fourPiFrame->GetXaxis()->SetTitleOffset(1.2);

  fourPiFrame->GetYaxis()->SetTitleFont(63);
  fourPiFrame->GetYaxis()->SetTitleSize(30);
  fourPiFrame->GetYaxis()->SetLabelFont(63);
  fourPiFrame->GetYaxis()->SetLabelSize(25);
  fourPiFrame->GetYaxis()->SetTitleOffset(.75);
 
  fourPiAGS->Draw("PZ");
  fourPiSPS->Draw("PZ");
  fourPiRHIC->Draw("PZ");

  

  TGraphErrors *fourPiYieldPlus = new TGraphErrors();
  TGraphErrors *fourPiYieldMinus = new TGraphErrors();

  fourPiYieldPlus->SetName("fourPiYieldPlus");
  fourPiYieldMinus->SetName("fourPiYieldMinus");

  fourPiYieldPlus->SetMarkerStyle(kOpenStar);
  fourPiYieldMinus->SetMarkerStyle(kFullStar);

  fourPiYieldPlus->SetMarkerSize(2.5);
  fourPiYieldMinus->SetMarkerSize(2.5);

  fourPiYieldPlus->SetMarkerColor(kRed);
  fourPiYieldMinus->SetMarkerColor(kRed);

  for (int iEnergy=0; iEnergy<nEnergies; iEnergy++){

    double energy = GetEnergy(iEnergy);
    std::pair<double,double> nPart = GetAverageNpart(energy,8); //8 is the 0-5% Centraltiy bin
    
    double ratioPlus = fourPiYieldPlusVals[iEnergy] / nPart.first;
    fourPiYieldPlus->SetPoint(iEnergy,energy*1.1,ratioPlus);
    fourPiYieldPlus->SetPointError(iEnergy,0,
				   ratioPlus * sqrt(pow(fourPiYieldPlusErrs[iEnergy]/fourPiYieldPlusVals[iEnergy],2) +
						    pow(nPart.second/nPart.first,2)));
    
    double ratioMinus = fourPiYieldMinusVals[iEnergy] / nPart.first;
    fourPiYieldMinus->SetPoint(iEnergy,energy,ratioMinus);
    fourPiYieldMinus->SetPointError(iEnergy,0,
				    ratioMinus * sqrt(pow(fourPiYieldMinusErrs[iEnergy]/fourPiYieldMinusVals[iEnergy],2) +
						      pow(nPart.second/nPart.first,2)));
    
  }
  
  fourPiYieldPlus->Draw("PZ");
  fourPiYieldMinus->Draw("PZ");
  
  TPaveText *rapidityFourPiTitle = new TPaveText(.12,.83,.50,.945,"NDC");
  rapidityFourPiTitle->SetBorderSize(0);
  rapidityFourPiTitle->SetFillColor(kWhite);
  rapidityFourPiTitle->SetTextFont(63);
  rapidityFourPiTitle->SetTextSize(35);
  rapidityFourPiTitle->SetTextAlign(12);
  rapidityFourPiTitle->AddText("Full Phase Space Yield");
  rapidityFourPiTitle->AddText("#scale[.8]{Most Central}");
  rapidityFourPiTitle->Draw();

  TLegend *fourPiLegend = new TLegend(*widthLegend);
  fourPiLegend->SetX1NDC(.12);
  fourPiLegend->SetY1NDC(.67);
  fourPiLegend->SetX2NDC(.58);
  fourPiLegend->SetY2NDC(.81);
  fourPiLegend->Draw("SAME");

  TPaveText *fourPiFooter = new TPaveText(*widthFooter);
  fourPiFooter->SetX1NDC(.1);
  fourPiFooter->Draw();
  
}
