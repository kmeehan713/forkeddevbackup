void FitWorldData(){

  //Create a canvas
  TCanvas *canvas = new TCanvas("canvas","canvas",20,20,800,600);
  TH1F *frame = canvas->DrawFrame(-5,0,5,300);
  
  //Load the Rapidity Density Distributions
  const int nWorldData(10);
  TGraphErrors *worldData[nWorldData];
  worldData[0] = new TGraphErrors("../WorldData/RapidityDensities/dndy_2gev_pim_e895.data","%lg %lg %lg");
  worldData[1] = new TGraphErrors("../WorldData/RapidityDensities/dndy_4gev_pim_e895.data","%lg %lg %lg");
  worldData[2] = new TGraphErrors("../WorldData/RapidityDensities/dndy_6gev_pim_e895.data","%lg %lg %lg");
  worldData[3] = new TGraphErrors("../WorldData/RapidityDensities/dndy_8gev_pim_e895.data","%lg %lg %lg");
  worldData[4] = new TGraphErrors("../WorldData/RapidityDensities/dndy_20gev_pim_na49.data","%lg %lg %lg");
  worldData[5] = new TGraphErrors("../WorldData/RapidityDensities/dndy_30gev_pim_na49.data","%lg %lg %lg");
  worldData[6] = new TGraphErrors("../WorldData/RapidityDensities/dndy_40gev_pim_na49.data","%lg %lg %lg");
  worldData[7] = new TGraphErrors("../WorldData/RapidityDensities/dndy_80gev_pim_na49.data","%lg %lg %lg");
  worldData[8] = new TGraphErrors("../WorldData/RapidityDensities/dndy_158gev_pim_na49.data","%lg %lg %lg");
  worldData[9] = new TGraphErrors("../WorldData/RapidityDensities/dndy_200gev_pim_brahms.data","%lg %lg %lg");


  //Fit With A simple Gaussian Function with mean fixed to zero
  TF1 *worldDataFits[nWorldData];
  for (int iData=0; iData<nWorldData; iData++){
    worldDataFits[iData] = new TF1(Form("%dFit",iData),"gaus(0)",-50,50);

    worldDataFits[iData]->SetParameter(0,10);
    worldDataFits[iData]->FixParameter(1,0.0);
    worldDataFits[iData]->SetParameter(2,1);

    worldData[iData]->Fit(worldDataFits[iData],"QR");

    cout <<iData <<" " <<worldDataFits[iData]->GetParameter(0) <<"+-" <<worldDataFits[iData]->GetParError(0)
	 <<"    " <<worldDataFits[iData]->GetParameter(2) <<"+-" <<worldDataFits[iData]->GetParError(2)
	 <<"    " <<worldDataFits[iData]->Integral(-50,50)<<"+-" <<worldDataFits[iData]->IntegralError(-50,50)
	 <<endl;
    
    worldData[iData]->Draw("*");
  }

}
