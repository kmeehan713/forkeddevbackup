#include "../inc/globalDefinitions.h"
#include "../inc/StyleSettings.h"
//#include "CanvasPartition.h"

void DrawCountdNdyAllEnergies(TString eventConfig, TString system, Int_t speciesIndex,
			      Int_t charge, Bool_t corrected=false){

  Bool_t save = true; //Should the canvas be saved?
  Double_t minmTm0=.25;        //Minimum to start count
  Double_t maxmTm0=.5;       //Maximum to count                                                          
  const int nEnergies(7);
  Double_t energies[nEnergies] = {7.7,11.5,14.5,19.6,27.0,39.0,62.4};

  const int nCentBins = 9;
  Double_t *yScaleMax; 
  Double_t yScaleMin[nCentBins] = {0, 0,  0,  0,  0,  0,  0,  0,  0};

  Double_t yScaleMaxPion[nCentBins] = {5, 8.5, 14.5, 23.5, 37.5, 60.5, 90.5, 120.5, 140.0};
  Double_t yScaleMaxKaon[nCentBins] = {1.5, 2, 4, 7, 9, 13, 20, 30, 35};
  Double_t yScaleMaxProtonPlus[nCentBins] = {.8, 1.6, 3, 4.5, 8, 11, 16, 22, 25};
  Double_t yScaleMaxProtonMinus[nCentBins] = {0.3, 0.5, 0.8, 1.2, 1.8, 2.5, 4, 5, 6.5};
  
  if (speciesIndex == 0)
    yScaleMax = yScaleMaxPion;
  else if (speciesIndex == 1)
    yScaleMax = yScaleMaxKaon;
  else if (speciesIndex == 2){
    if (charge < 0)
      yScaleMax = yScaleMaxProtonMinus;
    else
      yScaleMax = yScaleMaxProtonPlus;
  }
  

  gSystem->Load("../bin/utilityFunctions_cxx.so");
  gSystem->Load("../bin/TrackInfo_cxx.so");
  gSystem->Load("../bin/PrimaryVertexInfo_cxx.so");
  gSystem->Load("../bin/EventInfo_cxx.so");
  gSystem->Load("../bin/ParticleInfo_cxx.so");
  gSystem->Load("../bin/StRefMultExtendedCorr_cxx.so");
  gSystem->Load("../bin/UserCuts_cxx.so");
  gSystem->Load("../bin/SpectraFitFunctions_cxx.so");
  SetVariableUserCuts(7.7,eventConfig,"");
  ParticleInfo *particleInfo = new ParticleInfo();

  //Spectra Files                                                                                                    
  TFile *spectraFile[nEnergies];
  spectraFile[0] = new TFile(Form("../userfiles/AuAu07/outputFiles/AuAu07_%s_Spectra.root",eventConfig.Data()),"READ");
  spectraFile[1] = new TFile(Form("../userfiles/AuAu11/outputFiles/AuAu11_%s_Spectra.root",eventConfig.Data()),"READ");
  spectraFile[2] = new TFile(Form("../userfiles/AuAu14/outputFiles/AuAu14_%s_Spectra.root",eventConfig.Data()),"READ");
  spectraFile[3] = new TFile(Form("../userfiles/AuAu19/outputFiles/AuAu19_%s_Spectra.root",eventConfig.Data()),"READ");
  spectraFile[4] = new TFile(Form("../userfiles/AuAu27/outputFiles/AuAu27_%s_Spectra.root",eventConfig.Data()),"READ");
  spectraFile[5] = new TFile(Form("../userfiles/AuAu39/outputFiles/AuAu39_%s_Spectra.root",eventConfig.Data()),"READ");
  spectraFile[6] = new TFile(Form("../userfiles/AuAu62/outputFiles/AuAu62_%s_Spectra.root",eventConfig.Data()),"READ");

  //Create the Spectra Name                                                                                          
  TString type = "raw";
  TString Type = "Raw";
  TString name = "Uncorrected";
  if (corrected){
    type = "corrected";
    Type = "Corrected";
    name = "Corrected";
  }
  
  //Create the Canvas                                                                                                
  TCanvas *canvas = new TCanvas(Form("RapidityDensity_%s_%s_%s",system.Data(),
                                     particleInfo->GetParticleName(speciesIndex,charge).Data(),eventConfig.Data()),
                                "",5,20,1900,800);
  canvas->Divide(5,2,.0001,.001);
  
  canvas->cd(10);

  TPaveText *title = new TPaveText(.16,.7,.99,.95,"BRNDC");
  title->AddText("Measured Rapidity");
  title->AddText(Form("Densities of %s",particleInfo->GetParticleSymbol(speciesIndex,charge).Data()));
  title->AddText(Form("m_{T}-m_{%s}=[%.03g,%.03g] (GeV/c^{2})",particleInfo->GetParticleSymbol(speciesIndex).Data(),
		      minmTm0,maxmTm0));
  title->SetTextAlign(12);
  title->SetTextFont(63);
  title->SetTextSize(32);
  title->GetLine(2)->SetTextSize(20);
  title->SetFillColor(kWhite);
  title->Draw("SAME");
  
  TLegend *leg = new TLegend(.2,.11,.95,.69);
  leg->SetFillColor(kWhite);
  leg->SetBorderSize(0);
  leg->SetNColumns(2);
  leg->SetTextFont(63);
  leg->SetTextSize(25);
  leg->SetHeader("#sqrt{s_{NN}} (GeV)");
  for (int iEnergy=0; iEnergy<nEnergies; iEnergy++){
    TMarker *marker = new TMarker(0,0,kFullCircle);
    marker->SetMarkerColor(GetEnergyColor(energies[iEnergy]));
    marker->SetMarkerSize(2.0);
    leg->AddEntry(marker,Form("%.1f",energies[iEnergy]),"P");
  }
  leg->Draw();


  //Loop Over the Pads and Draw
  for (int iPadX=0; iPadX<nCentBins; iPadX++){
    
    int iCentBin = iPadX;
    TString speciesName = particleInfo->GetParticleName(speciesIndex,charge);
    
    canvas->cd(iPadX+1);
    gPad->SetTicks(1,1);
    gPad->SetRightMargin(.005);
    gPad->SetTopMargin(.02);
    gPad->SetLeftMargin(.2);

    TH1F *frame = gPad->DrawFrame(-1.35,yScaleMin[iCentBin],1.35,yScaleMax[iCentBin]);
    frame->GetXaxis()->SetTitle(Form("y_{%s}",
				     particleInfo->GetParticleSymbol(speciesIndex).Data()));
    frame->GetYaxis()->SetTitleOffset(1.5);
    frame->GetYaxis()->SetTitle("dN/dy");
    
    frame->GetXaxis()->SetTitleFont(63);
    frame->GetXaxis()->SetTitleSize(20);
    frame->GetXaxis()->SetLabelSize(18);
    frame->GetXaxis()->SetLabelFont(63);
    frame->GetXaxis()->SetTitleOffset(1.8);

    frame->GetYaxis()->SetTitleFont(63);
    frame->GetYaxis()->SetTitleSize(20);
    frame->GetYaxis()->SetLabelSize(18);
    frame->GetYaxis()->SetLabelFont(63);
    frame->GetYaxis()->SetTitleOffset(2.8);

    TPaveText *centTitle = new TPaveText(.67,.83,.94,.94,"NDC");
    centTitle->SetFillColor(kWhite);
    centTitle->SetBorderSize(0);
    centTitle->SetTextSize(25);
    centTitle->SetTextFont(63);
    centTitle->SetTextAlign(32);
    centTitle->AddText(Form("%d-%d%%",
			    iCentBin!=nCentBins-1 ? (int)GetCentralityPercents().at(iCentBin+1):0,
			    (int)GetCentralityPercents().at(iCentBin)));

    centTitle->Draw("SAME");
    
    gPad->Modified();
    gPad->Update();
    canvas->Update();
    
    //continue;
    //Each xPad represents a centrality bin. For each centrality
    //loop over all energies and create a dNdy plot
    for (int iEnergy=0; iEnergy<nEnergies; iEnergy++){
      
      TGraphErrors *gr = new TGraphErrors();
      gr->SetMarkerStyle(kFullCircle);
      gr->SetMarkerColor(GetEnergyColor(energies[iEnergy]));
      
      //Loop Over the Rapidity Bins
      for (int yIndex = GetMinRapidityIndexOfInterest(speciesIndex); yIndex<=GetMaxRapidityIndexOfInterest(speciesIndex); yIndex++){
	
	//Get the Spectrum
	TGraphErrors *spectra=NULL, *spectraTOF=NULL;
	
	TString spectraName(Form("%sSpectra_%s_Cent%02d_yIndex%02d",
				 type.Data(),particleInfo->GetParticleName(speciesIndex,charge).Data(),
				 iCentBin,yIndex));
	TString spectraNameTOF(Form("%sSpectraTOF_%s_Cent%02d_yIndex%02d",
				    type.Data(),particleInfo->GetParticleName(speciesIndex,charge).Data(),
				    iCentBin,yIndex));
	
	spectra = (TGraphErrors *)spectraFile[iEnergy]->
	  Get(Form("%sSpectra_%s/%s",Type.Data(),
		   particleInfo->GetParticleName(speciesIndex,charge).Data(),
		   spectraName.Data()));
	spectraTOF = (TGraphErrors *)spectraFile[iEnergy]->
	  Get(Form("%sSpectra_%s/%s",Type.Data(),
		   particleInfo->GetParticleName(speciesIndex,charge).Data(),
		   spectraNameTOF.Data()));
	
	cout <<iCentBin <<" " <<iEnergy <<" " <<yIndex <<" " <<spectra <<" " <<spectraTOF <<endl;
	if (!spectra)
	  continue;
	
	std::pair<double,double> tpcCount = CountdNdyOfSpectrum(spectra,speciesIndex,minmTm0,maxmTm0);
	std::pair<double,double> tofCount = (spectraTOF != NULL ? CountdNdyOfSpectrum(spectraTOF,speciesIndex,minmTm0,maxmTm0):
					     std::make_pair(0.0,0.0));	  
	Double_t dNdyMeasured = tpcCount.first + tofCount.first;
	Double_t dNdyErr = sqrt(pow(tpcCount.second,2) + pow(tpcCount.second,2));
	
	gr->SetPoint(gr->GetN(),GetRapidityRangeCenter(yIndex),dNdyMeasured);
	gr->SetPointError(gr->GetN()-1,rapidityBinWidth/2.0,dNdyErr);
	
	if (spectra)
	  delete spectra;
	if (spectraTOF)
	  delete spectraTOF;
	
      }//End Loop Over Rapidity Bins
      
      gr->Draw("P");
      
    }//End Loop Over Energies
    
    
    gPad->Modified();
    gPad->Update();
    canvas->Update();
    

  }//End Loop Over iPadX

  if (save){
    canvas->Print(Form("%s.gif",canvas->GetName()));
  }

}
