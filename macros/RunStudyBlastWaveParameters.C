//This macro runs the src/simultaneousBlastWaveFit code and performs
//a simultaneous blast wave fit to the pi+-/k+-/p+- spectra
//to extract the dNdy

Bool_t drawSpectra = true; //Should the spectra be drawn? This overrides the values in the source code.

void RunStudyBlastWaveParameters(TString spectraFileName, TString outBlastWaveFileName,
				 TString starLibrary, Double_t energy, TString eventConfig){
  
  
  //Load the necessary Librarires
  gSystem->Load("../bin/utilityFunctions_cxx.so");
  gSystem->Load("../bin/TrackInfo_cxx.so");
  gSystem->Load("../bin/PrimaryVertexInfo_cxx.so");
  gSystem->Load("../bin/EventInfo_cxx.so");
  gSystem->Load("../bin/DavisDstReader_cxx.so");
  gSystem->Load("../bin/StRefMultExtendedCorr_cxx.so");
  gSystem->Load("../bin/UserCuts_cxx.so");
  gSystem->Load("../bin/ParticleInfo_cxx.so");
  gSystem->Load("../bin/SpectraFitFunctions_cxx.so");
  gSystem->Load("../bin/SpectraClass_cxx.so");
  gSystem->Load("../bin/SimFitter_cxx.so");
  gSystem->Load("../bin/StudyBlastWaveParameters_cxx.so");

  draw = drawSpectra;

  SetVariableUserCuts(energy,eventConfig,starLibrary);

  StudyBlastWaveParameters(spectraFileName,outBlastWaveFileName,energy);
  
}
