void FindLambdas(){

  gSystem->Load("../bin/TrackInfo_cxx.so");
  gSystem->Load("../bin/PrimaryVertexInfo_cxx.so");
  gSystem->Load("../bin/EventInfo_cxx.so");
  gSystem->Load("../bin/DavisDstReader_cxx.so");
  gSystem->Load("../bin/ParticleInfo_cxx.so");
  gSystem->Load("../bin/StRefMultExtendedCorr_cxx.so");
  gSystem->Load("../bin/UserCuts_cxx.so");
  
  SetVariableUserCuts(62.4,"ColliderCenter","SL10k");
  ParticleInfo *particleInfo = new ParticleInfo("SL10k",true);
  
  DavisDstReader *a = new DavisDstReader("/home/chris/Documents/davisdstanalysis/userfiles/AuAu62/fileList/adc/AuAu62_adc_0.list",-30,30);

  EventInfo *event = NULL;
  PrimaryVertexInfo *vertex = NULL;
  TrackInfo *track = NULL;
  
  cout <<a->GetEntries() <<endl;

  TH1D *lambdaInvMassHisto = new TH1D("lambdaInvMassHisto","lambdaInvMassHisto;InvMass(p,#pi^{-})",200,1.0,1.25);
  TH2D *protondEdxHisto = new TH2D("protondEdxHisto","protondEdxHisto;p_{Total};dE/dx",200,0,2,200,0,60);
  TH2D *piondEdxHisto = new TH2D("piondEdxHisto","piondEdxHisto;p_{Total};dE/dx",200,0,2,200,0,10);

  for (long iEntry=0; iEntry<a->GetEntries(); iEntry++){
    
    event = a->GetEntry(iEntry);
    if (!IsGoodEvent(event))
      continue;

    vertex = event->GetPrimaryVertex(0);
    if (!IsGoodVertex(vertex))
      continue;

    //Find All the Protons, they must pass the good track cuts
    std::vector<int> protons;
    for (int iTrack=0; iTrack<vertex->GetNPrimaryTracks(); iTrack++){
      track = vertex->GetPrimaryTrack(iTrack);
      if (!IsGoodTrack(track))
	continue;

      if (track->GetGlobalDCA()->Mag() < 0.3)
	continue;
      
      if (particleInfo->IsExclusivelyThisSpeciesTPC(track->GetdEdx()*pow(10,6),PROTON,track->GetPTotal(),2*7.0,false) && track->GetCharge() > 0){
	protons.push_back(iTrack);
	protondEdxHisto->Fill(track->GetPTotal(),track->GetdEdx()*pow(10,6));
      }
      
    }//End Find Protons

    //Find All the Pions, they DO NOT need to pass the good track cuts
    std::vector<int> pions;
    for (int iTrack=0; iTrack<vertex->GetNPrimaryTracks(); iTrack++){
      track = vertex->GetPrimaryTrack(iTrack);

      if (track->GetGlobalDCA()->Mag() < 1.3)
	continue;

      if (particleInfo->IsExclusivelyThisSpeciesTPC(track->GetdEdx()*pow(10,6),PION,track->GetPTotal(),2*7.0,false) && track->GetCharge() < 0){
	pions.push_back(iTrack);
	piondEdxHisto->Fill(track->GetPTotal(),track->GetdEdx()*pow(10,6));
      }
    }//End Find Pions

    //Loop Over all the protons
    for (unsigned int iProton=0; iProton<protons.size(); iProton++){

      TrackInfo *protonTr = vertex->GetPrimaryTrack(protons.at(iProton));

      TVector3 protonMom(protonTr->GetPt()*TMath::Cos(protonTr->GetPhi()),protonTr->GetPt()*TMath::Sin(protonTr->GetPhi()),protonTr->GetPz());
      TLorentzVector protonFourVec(protonMom.X(),protonMom.Y(),protonMom.Z(),sqrt(pow(particleInfo->GetParticleMass(PROTON),2) + pow(protonMom.Mag(),2)));

      //Loop Over all of the pions and construct the invariant mass
      for (unsigned int iPion=0; iPion<pions.size(); iPion++){

	TrackInfo *pionTr = vertex->GetPrimaryTrack(pions.at(iPion));

	TVector3 pionMom(pionTr->GetPt()*TMath::Cos(pionTr->GetPhi()),pionTr->GetPt()*TMath::Sin(pionTr->GetPhi()),pionTr->GetPz());
	TLorentzVector pionFourVec(pionMom.X(),pionMom.Y(),pionMom.Z(),sqrt(pow(particleInfo->GetParticleMass(PION),2) + pow(pionMom.Mag(),2)));

	TLorentzVector pair = protonFourVec + pionFourVec;
	Double_t invMass = pair.M();

	lambdaInvMassHisto->Fill(invMass);
	
      }//End Loop Over Pions

    }//End Loop Over Protons
    
  }//End Loop Over Entries

  TCanvas *canvas = new TCanvas("canvas","canvas",20,20,1200,600);
  canvas->Divide(3,1);
  canvas->cd(1);
  lambdaInvMassHisto->Draw();
  canvas->cd(2);
  protondEdxHisto->Draw("COLZ");
  canvas->cd(3);
  piondEdxHisto->Draw("COLZ");
  
}
