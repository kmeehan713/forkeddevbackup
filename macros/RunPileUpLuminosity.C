//This macro runs the compiled pileUpLuminosity binary
void RunPileUpLuminosity(TString fitFile="", TString dataFile="", TString glauberFile="", TString outputFile="", Double_t energy, TString eventConfig, TString starLibrary, 
												Int_t normStartBin, Int_t normEndBin, Double_t NegativeBinomialParameterNPP, Double_t NegativeBinomialParameterK, 
												Double_t pileUpEventOccurence, Int_t nSimulatedEvents, Bool_t drawCanvas){

	//Load the necessary libraries
	gSystem->Load("../bin/TrackInfo_cxx.so");
	gSystem->Load("../bin/PrimaryVertexInfo_cxx.so");
	gSystem->Load("../bin/EventInfo_cxx.so");
	gSystem->Load("../bin/DavisDstReader_cxx.so");
	gSystem->Load("../bin/ParticleInfo_cxx.so");
	gSystem->Load("../bin/StRefMultExtendedCorr_cxx.so");
	
	gSystem->Load("../bin/GlauberClass_cxx.so");
	gSystem->Load("../bin/GlauberModel_cxx.so");  

	gSystem->Load("../bin/UserCuts_cxx.so");
	gSystem->Load("../bin/pileupLuminosity_cxx.so");

  //Set the User Cuts
  SetVariableUserCuts(energy,eventConfig,starLibrary);

	pileupLuminosity(fitFile, dataFile, glauberFile, outputFile, normStartBin, normEndBin, NegativeBinomialParameterNPP, NegativeBinomialParameterK, pileUpEventOccurence, nSimulatedEvents, drawCanvas);

}
